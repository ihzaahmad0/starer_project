@extends('layouts.master')

@section('page_title', 'User Config')
@section('page_sub_title', 'User')

@section('breadcrumb')
    @php
        $breadcrumbs = ['Pengaturan User', ['User', route('admin.user_config.user.index')], 'Edit'];
    @endphp
    @include('layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
@endsection

@section('content')
    <div class="row mb-3">
        <div class="col-md-12">
            <form action="{{ route('admin.user_config.user.update', ['id' => $data['obj']->id]) }}" method="POST"
                autocomplete="off" class="card-body" enctype="multipart/form-data">
                <div class="card">
                    <div class="card-header">
                        Edit User {{ $data['obj']->name }}
                    </div>
                    @csrf
                    <div class="card-body">
                        @include($view . 'components.detail')
                        <x-forms.input-image-with-preview img-preview-id="profile_image" label="Foto Profile"
                            is-required="FALSE" input-name="profile_image" :img-src="$data['obj']->image('profile_image')
                                ? $data['obj']->image('profile_image')
                                : asset('assets/img/avatars/default.png')" class="mt-3" />
                        <div class="form-group mt-4">
                            <label for="name">Status <span class="text-danger">*</span></label>
                            <br>
                            <div class="btn-group" role="group" aria-label="Basic radio toggle button group">

                                <input type="radio" class="btn-check" name="active" value="1" id="btn_active"
                                    {{ $data['obj']->active ? 'checked' : '' }}>
                                <label class="btn btn-outline-success" for="btn_active">Aktif</label>

                                <input type="radio" class="btn-check" name="active" value="0" id="btn_non_active"
                                    {{ $data['obj']->active ? '' : 'checked' }}>
                                <label class="btn btn-outline-danger" for="btn_non_active">Non Aktif</label>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-muted text-center">
                        <button class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @include('layouts.general_informations.userResponsibleStamp', ['data' => $data['obj']])
@endsection

@include('layouts.select2.init')
@push('scripts')
    <script>
        $('.select2').select2();

        $("#username").attr('disabled', true);
    </script>
@endpush
@push('scripts')
    <script src="{{ asset('assets/js/select2/ajax_data_pagination.js') }}"></script>
    <script>
        $(document).ready(function() {
            ajax_select2_pagination("#entitas_id", "{{ route('master.select2.entitas.') }}");
        });
    </script>
@endpush
