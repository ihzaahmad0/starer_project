@extends('layouts.master')

@section('page_title', 'User Config')
@section('page_sub_title', 'User')

@section('breadcrumb')
    @php
        $breadcrumbs = ['Pengaturan User', ['User', route('admin.user_config.user.index')]];
    @endphp
    @include('layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex">
                    <div class="head-label text-center">
                        <h5 class="card-title mb-0">List User</h5>
                    </div>
                    <div class="card-tools ms-auto">
                        @can('create user_config.user')
                            <a class="btn btn-primary btn-sm" href="{{ route('admin.user_config.user.createGet') }}"
                                data-bs-toggle="tooltip" data-bs-title="Add Data"><i class="fa fa-plus" aria-hidden="true"></i>
                                Add</a>
                        @endcan
                    </div>
                    <!-- /.card-tools -->
                </div>
                <div class="card-body table-responsive text-nowrap">
                    <table class="table table-bordered" id="main-table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Created At</th>
                                <th>Status</th>
                                <th style="width: 7%">Aksi</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th>
                                    @include('layouts.datatables.filter.input', [
                                        'name' => 'name[il]',
                                        'placeholder' => 'nama...',
                                    ])
                                </th>
                                <th>
                                    @include('layouts.datatables.filter.input', [
                                        'name' => 'email[il]',
                                        'placeholder' => 'email...',
                                    ])
                                </th>
                                <th>
                                    @include('layouts.datatables.filter.datepicker', [
                                        'name' => 'created_at[e]',
                                    ])
                                </th>
                                <th>
                                    @include('layouts.datatables.filter.select', [
                                        'name' => 'active[e]',
                                        'options' => [
                                            '1' => 'Aktif',
                                            '0' => 'Non Aktif',
                                        ],
                                    ])
                                </th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('layouts.data_tables.basic_data_tables')
@include('layouts.select2.init')
@include('layouts.images_viewer.lightbox')
@push('scripts')
    <script src="{{ asset('assets/js/datatables/init_datatable.js') }}"></script>
    <script src="{{ asset('assets/js/fetch_api/fetchPost.js') }}"></script>
    <script src="{{ asset('assets/js/utils/btn-delete-item-datatable.js') }}"></script>

    <script>
        $main_datatable = init_serverside_datatable(
            '#main-table',
            "{{ route($route . 'datatable') }}",
            [{
                    data: 'name',
                },
                {
                    data: 'email',
                },
                {
                    data: 'created_at',
                },
                {
                    data: 'status',
                    sortable: false
                },
                {
                    data: 'action',
                    sortable: false,
                    className: "text-center"
                }
            ], {
                order: [3, 'desc'],
            }
        );
    </script>
@endpush
