@extends('layouts.master')

@section('page_title', 'User Config')
@section('page_sub_title', 'User')

@section('breadcrumb')
    @php
        $breadcrumbs = ['Pengaturan User', ['User', route('admin.user_config.user.index')], 'Detail'];
    @endphp
    @include('layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Detail User
                </div>
                <div class="card-body">
                    <fieldset disabled>
                        @include($view . 'components.detail')
                        <x-forms.input-image-with-preview img-preview-id="profile_image" label="Foto Profile"
                            is-required="FALSE" input-name="profile_image" :img-src="$data['obj']->image('profile_image')
                                ? $data['obj']->image('profile_image')
                                : asset('assets/img/avatars/default.png')" class="mt-3" />
                        <div class="form-group mt-4">
                            <label for="name">Status <span class="text-danger">*</span></label>
                            <br>
                            <div class="btn-group" role="group" aria-label="Basic radio toggle button group">

                                <input type="radio" class="btn-check" name="active" value="1" id="btn_active"
                                    {{ $data['obj']->active ? 'checked' : '' }}>
                                <label class="btn btn-outline-success" for="btn_active">Aktif</label>

                                <input type="radio" class="btn-check" name="active" value="0" id="btn_non_active"
                                    {{ $data['obj']->active ? '' : 'checked' }}>
                                <label class="btn btn-outline-danger" for="btn_non_active">Non Aktif</label>
                            </div>
                        </div>
                    </fieldset>
                </div>
                @if ($data['obj']->id !== auth()->user()->id)
                    <div class="card-footer text-muted text-center">
                        @if ($data['obj']->trashed())
                            @can('restore user_config.user')
                                <a class="btn btn-danger btn-info"
                                    href="{{ route('admin.user_config.user.restore', ['id' => $data['obj']->id]) }}"
                                    data-bs-toggle="tooltip" data-placement="top" data-bs-title="Hapus"
                                    onclick="return confirm('Yakin Mengembalikan?')"><i class="far fa-trash-alt"></i>
                                    Kembalikan</a>
                            @endcan
                        @else
                            @can('Super-Admin')
                                <a class="btn btn-warning"
                                    href="{{ route('admin.user_config.user.login.as.user', ['id' => $data['obj']->id]) }}"
                                    data-bs-toggle="tooltip" data-placement="top"
                                    data-bs-title="Login Sebagai {{ $data['obj']->name }}"
                                    onclick="return confirm('Yakin Login Sebagai {{ $data['obj']->name }}')"><i
                                        class="fa fa-key"></i> Login</a>
                            @endcan
                        @endif
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $("#password").closest(".form-group").hide();
        $("#role").removeAttr("multiple");
    </script>
@endpush
