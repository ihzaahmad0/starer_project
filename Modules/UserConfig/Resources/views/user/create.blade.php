@extends('layouts.master')

@section('page_title', 'User Config')
@section('page_sub_title', 'User')

@section('breadcrumb')
    @php
        $breadcrumbs = ['Pengaturan User', ['User', route('admin.user_config.user.index')], 'Tambah'];
    @endphp
    @include('layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Tambah User Baru
                </div>
                <form action="{{ route('admin.user_config.user.createPost') }}" method="POST" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        @include($view . 'components.detail')
                        <x-forms.input-image-with-preview img-preview-id="profile_image" label="Foto Profile"
                            is-required="FALSE" input-name="profile_image" :img-src="asset('assets/img/avatars/default.png')" class="mt-3" />
                    </div>
                    <div class="card-footer text-muted text-center">
                        <button class="btn btn-primary"><i class="fas fa-save"></i> Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@include('layouts.select2.init')
@push('scripts')
    <script>
        $('.select2').select2({
            placeholder: 'Pilih Peran User...'
        });
    </script>
@endpush
@push('scripts')
    <script src="{{ asset('assets/js/select2/ajax_data_pagination.js') }}"></script>
    <script>
        $(document).ready(function() {
            ajax_select2_pagination("#entitas_id", "{{ route('master.select2.entitas.') }}");
        });
    </script>
@endpush
