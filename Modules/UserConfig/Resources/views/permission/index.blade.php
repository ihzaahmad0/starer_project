@extends('layouts.master')

@section('page_title', 'User Config')
@section('page_sub_title', 'Role')

@section('breadcrumb')
    @php
        $breadcrumbs = ['Pengaturan User', ['Perizinan', route('admin.user_config.role.index')]];
    @endphp
    @include('layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card bg-secondary-300">
                <div class="card-header d-flex">
                    <div class="head-label text-center">
                        <h5 class="card-title mb-0">List Role</h5>
                    </div>
                    <div class="card-tools ms-auto">
                        @can('create user_config.role')
                            <a class="btn btn-sm btn-primary" href="{{ route('admin.user_config.role.createGet') }}"
                                data-bs-toggle="tooltip" data-bs-title="Add Data"><i class="fa fa-plus" aria-hidden="true"></i>
                                Add</a>
                        @endcan
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="main-table">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Terakhir Update</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('layouts.data_tables.basic_data_tables')

@push('scripts')
    <script>
        $(function() {
            $('#main-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! url()->full() !!}',
                columns: [{
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'created_at',
                        name: 'created_at'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        className: "text-center"
                    }
                ]
            });
        });
    </script>
@endpush
