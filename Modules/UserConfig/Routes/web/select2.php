<?php

use App\Models\BaseModel;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth'])->prefix('user_config/select2')->name('user_config.select2.')->group(function () {
    Route::match(['get', 'post'], 'user', function () {
        return select2_route(User::class);
    })->name('user');
});
