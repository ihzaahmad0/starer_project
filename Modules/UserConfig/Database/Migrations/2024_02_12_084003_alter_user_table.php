<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Master\Entities\Entitas;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // add column to table user
        Schema::table(User::getTableName(), function (Blueprint $table) {
            // active
            $table->boolean('active')->default(1)->after('email_verified_at');
            // profile image
            $table->string('profile_image')->nullable()->after('active');
            // entitas_id
            $table->foreignId('entitas_id')->nullable()->constrained()->on(Entitas::getTableName());
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table(User::getTableName(), function (Blueprint $table) {
            $table->dropColumn('active');
            $table->dropColumn('profile_image');
            $table->dropColumn('entitas_id');
        });
    }
};
