<?php

namespace Modules\Master\Http\Requests\StatusKaryawan;

use App\Rules\UniqueSoftDelete;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Master\Entities\StatusKaryawan;

class UpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'nama' => ['required', new UniqueSoftDelete(StatusKaryawan::class, request()->route('id'))],
        ];
    }

    public function attributes()
    {
        return [
            'kode' => 'Kode Status Karyawan',
            'nama' => 'Nama Status Karyawan',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
