<?php

namespace Modules\Master\Http\Requests\Satuan;

use App\Rules\UniqueSoftDelete;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Master\Entities\Satuan;

class UpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'nama' => ['required', new UniqueSoftDelete(Satuan::class, request()->route('id'))],
        ];
    }

    public function attributes()
    {
        return [
            'kode' => 'Kode Satuan',
            'nama' => 'Nama Satuan',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
