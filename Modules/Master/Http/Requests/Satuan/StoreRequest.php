<?php

namespace Modules\Master\Http\Requests\Satuan;

use App\Rules\UniqueSoftDelete;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Master\Entities\Satuan;

class StoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'kode' => ['required', new UniqueSoftDelete(Satuan::class)],
            'nama' => ['required', new UniqueSoftDelete(Satuan::class)],
        ];
    }

    public function attributes()
    {
        return [
            'kode' => 'Kode Satuan',
            'nama' => 'Nama Satuan',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
