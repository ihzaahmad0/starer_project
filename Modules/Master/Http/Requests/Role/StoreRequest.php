<?php

namespace Modules\Master\Http\Requests\Role;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Master\Entities\Role;

class StoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'nama' => ['required', 'unique:' . Role::getTableName()],
            'keterangan' => ['required', 'unique:' . Role::getTableName()],
            'status' => ['required', 'unique:' . Role::getTableName()],
        ];
    }

    public function attributes()
    {
        return [
            'nama' => 'Nama Role',
            'keterangan' => 'Keterangan Role',
            'status' => 'Status Role',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
