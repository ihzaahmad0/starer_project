<?php

namespace Modules\Master\Http\Requests\Role;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Master\Entities\Jab
use Modules\Master\Entities\Role;

class UpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'nama' => ['required', 'unique:' . Role::getTableName()->ignore($this->id)],
            'keterangan' => ['required', 'unique:' . Role::getTableName()->ignore($this->id)],
            'status' => ['required', 'unique:' . Role::getTableName()->ignore($this->id)],
        ];
    }

    public function attributes()
    {
        return [
            'nama' => 'Nama Role',
            'keterangan' => 'Keterangan Role',
            'status' => 'Status Role',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
