<?php

namespace Modules\Master\Http\Requests\Utilitas;

use App\Rules\UniqueSoftDelete;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Modules\Master\Entities\Utilitas;

class UpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'nama' => ['required', new UniqueSoftDelete(Utilitas::class, request()->route('id'))],
            'satuan_id' => ['required']
        ];
    }

    public function attributes()
    {
        return [
            'kode' => 'Kode Utilitas',
            'nama' => 'Nama Utilitas',
            'satuan_id' => 'Satuan'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
