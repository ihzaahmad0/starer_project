<?php

namespace Modules\Master\Http\Requests\Supplier;

use App\Rules\UniqueSoftDelete;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Modules\Master\Entities\Supplier;

class UpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'singkatan' => ['required', new UniqueSoftDelete(Supplier::class, request()->route('id'))],
            'nama' => ['required', new UniqueSoftDelete(Supplier::class, request()->route('id'))],
            'layanan_id' => ['required']
        ];
    }

    public function attributes()
    {
        return [
            'kode' => 'Kode Supplier',
            'singkatan' => 'Singkatan Supplier',
            'nama' => 'Nama Supplier',
            'layanan_id' => 'Kode Layanan'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
