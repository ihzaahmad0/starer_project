<?php

namespace Modules\Master\Http\Requests\Supplier;

use App\Rules\UniqueSoftDelete;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Master\Entities\Supplier;

class StoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'kode' => ['required', new UniqueSoftDelete(Supplier::class)],
            'singkatan' => ['required', new UniqueSoftDelete(Supplier::class)],
            'nama' => ['required', new UniqueSoftDelete(Supplier::class)],
            'layanan_id' => ['required']
        ];
    }

    public function attributes()
    {
        return [
            'kode' => 'Kode Supplier',
            'singkatan' => 'Singkatan Supplier',
            'nama' => 'Nama Supplier',
            'layanan_id' => 'Kode Layanan'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
