<?php

namespace Modules\Master\Http\Requests\Layanan;

use App\Rules\UniqueSoftDelete;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Master\Entities\Layanan;

class StoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'kode' => ['required', new UniqueSoftDelete(Layanan::class)],
            'nama' => ['required', new UniqueSoftDelete(Layanan::class)],
        ];
    }

    public function attributes()
    {
        return [
            'kode' => 'Kode Layanan',
            'nama' => 'Nama Layanan',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
