<?php

namespace Modules\Master\Http\Requests\Entitas;

use App\Rules\UniqueSoftDelete;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Master\Entities\Entitas;

class StoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'nama' => ['required', new UniqueSoftDelete(Entitas::class)],
            'singkatan' => ['required', new UniqueSoftDelete(Entitas::class)],
            'kode' => ['required', new UniqueSoftDelete(Entitas::class)],
            'alamat' => ['required', new UniqueSoftDelete(Entitas::class)],
            'kota' => ['required', new UniqueSoftDelete(Entitas::class)],
            'entitas_image' => ['nullable', 'file', 'image', 'max:5120']
        ];
    }

    public function attributes()
    {
        return [
            'nama' => 'Nama Entitas',
            'singkatan' => 'Singkatan Entitas',
            'kode' => 'Kode Entitas',
            'alamat' => 'Alamat Entitas',
            'kota' => 'Kota Entitas',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
