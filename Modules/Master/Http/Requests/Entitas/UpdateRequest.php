<?php

namespace Modules\Master\Http\Requests\Entitas;

use App\Rules\UniqueSoftDelete;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Master\Entities\Jab;
use Modules\Master\Entities\Entitas;

class UpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'nama' => ['required', new UniqueSoftDelete(Entitas::class, request()->route('id'))],
            'singkatan' => ['required', new UniqueSoftDelete(Entitas::class, request()->route('id'))],
            'alamat' => ['required', new UniqueSoftDelete(Entitas::class, request()->route('id'))],
            'kota' => ['required', new UniqueSoftDelete(Entitas::class, request()->route('id'))],
            'entitas_image' => ['nullable', 'file', 'image', 'max:5120']
        ];
    }

    public function attributes()
    {
        return [
            'nama' => 'Nama Entitas',
            'singkatan' => 'Singkatan Entitas',
            'alamat' => 'Alamat Entitas',
            'kota' => 'Kota Entitas',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
