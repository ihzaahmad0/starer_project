<?php

namespace Modules\Master\Http\Requests\Identitas;

use App\Rules\UniqueSoftDelete;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Master\Entities\Identitas;

class UpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'nama' => ['required', new UniqueSoftDelete(Identitas::class, request()->route('id'))],
        ];
    }

    public function attributes()
    {
        return [
            'kode' => 'Kode Identitas',
            'nama' => 'Nama Identitas',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
