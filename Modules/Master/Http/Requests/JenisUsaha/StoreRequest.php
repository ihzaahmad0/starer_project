<?php

namespace Modules\Master\Http\Requests\JenisUsaha;

use App\Rules\UniqueSoftDelete;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Master\Entities\JenisUsaha;

class StoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'kode' => ['required', new UniqueSoftDelete(JenisUsaha::class)],
            'nama' => ['required', new UniqueSoftDelete(JenisUsaha::class)],
        ];
    }

    public function attributes()
    {
        return [
            'kode' => 'Kode Jenis Usaha',
            'nama' => 'Nama Jenis Usaha',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
