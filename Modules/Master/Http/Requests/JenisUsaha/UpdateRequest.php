<?php

namespace Modules\Master\Http\Requests\JenisUsaha;

use App\Rules\UniqueSoftDelete;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Master\Entities\JenisUsaha;

class UpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'nama' => ['required', new UniqueSoftDelete(JenisUsaha::class, request()->route('id'))],
        ];
    }

    public function attributes()
    {
        return [
            'kode' => 'Kode Jenis Usaha',
            'nama' => 'Nama Jenis Usaha',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
}
