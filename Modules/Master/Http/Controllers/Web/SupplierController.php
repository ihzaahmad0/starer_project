<?php

namespace Modules\Master\Http\Controllers\Web;

use App\Http\Controllers\BaseController;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Modules\Master\Entities\Layanan;
use Modules\Master\Http\Requests\Supplier\StoreRequest;
use Modules\Master\Http\Requests\Supplier\UpdateRequest;
use Modules\Master\Repositories\SupplierRepository;

class SupplierController extends BaseController
{
    protected $supplier_repository;
    public function __construct(SupplierRepository $supplier_repository)
    {
        $this->route = 'master.supplier.';
        $this->view = 'master::supplier.';
        $this->permission = 'master.supplier';
        $this->supplier_repository = $supplier_repository;
        $this->supplier_repository->setProperty($this->getPropertyToRepository());
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return $this->view('index');
    }

    public function datatable()
    {

        $query = $this->supplier_repository->getData();
        return datatables()->of($query)
            ->addColumn('action', function ($obj) {
                $button = '<a class="px-1 text-success btn-ajax-open-model-lg" href="' . route($this->route . 'show', ['id' => $obj->id]) . '" data-bs-toggle="tooltip" data-placement="top" data-bs-title="Lihat Detail"><i class="far fa-eye"></i></a>';
                // if user can update
                if (auth()->user()->can('update ' . $this->permission)) {
                    $button .= '<a class="px-1 text-info btn-ajax-open-model-lg" href="' . route($this->route . 'edit', ['id' => $obj->id]) . '" data-bs-toggle="tooltip" data-placement="top" data-bs-title="Edit"><i class="far fa-edit"></i></a>';
                }
                // if user can delete
                if (auth()->user()->can('delete ' . $this->permission)) {
                    $button .= '<a class="px-1 text-danger btn-delete-item-datatable" data-datatable-id="main-table" href="' . route($this->route . 'delete', ['id' => $obj->id]) . '" data-bs-toggle="tooltip" data-placement="top" data-bs-title="Hapus"><i class="fa fa-trash"></i></a>';
                }

                return $button;
            })
            ->editColumn('created_at', function ($obj) {
                return $obj->getFormattedDate('created_at');
            })
            ->editColumn('created_by', function ($obj) {
                return $obj->creator_name();
            })
            ->rawColumns(['action'])
            ->make(true);
        return;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return $this->view('create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $validated = $request->validated();
        $this->supplier_repository->storeFromRequest($validated);

        return response()->json(['status' => true, 'message' => 'Sukses menyimpan data'], 201);
    }

    /**
     * Show the specified resource.
     */
    public function show($id)
    {
        $data['obj'] = $this->supplier_repository->getById($id);
        return $this->view('show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $data['obj'] = $this->supplier_repository->getById($id);
        return $this->view('edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, $id)
    {
        $validated = $request->validated();
        $this->supplier_repository->updateFromRequest($id, $validated);

        return response()->json(['status' => true, 'message' => 'Sukses menyimpan data'], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function delete($id)
    {
        $this->supplier_repository->destroy($id);
        return response()->json(['status' => true, 'message' => 'Sukses menghapus data'], 200);
    }
}
