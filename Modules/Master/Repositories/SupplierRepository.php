<?php

namespace Modules\Master\Repositories;

use App\Repositories\BaseRepository;
use Modules\Master\Entities\Supplier;

class SupplierRepository extends BaseRepository
{
    public function __construct(Supplier $model)
    {
        $this->obj = $model;
    }

    public function  getData()
    {
        $validated = request()->validate([
            'kode' => 'nullable|array',
            'singkatan' => 'nullable|array',
            'nama' => 'nullable|array',
            'created_at' => 'nullable|array',
            'created_at.*' => 'nullable|date_format:d-m-Y',
            'layanan_id' => 'nullable|array',
            'created_by' => 'nullable|array',
        ]);

        $query = $this->obj
            ->with('layanan:id,nama')
            ->when($validated != [], function ($q) use ($validated) {
                filterData($q, $validated);
            })
            ->select($this->obj->getTable() . '.*');

        return $query;
    }
}
