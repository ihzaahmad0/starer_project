<?php

namespace Modules\Master\Repositories;

use App\Repositories\BaseRepository;
use Modules\Master\Entities\Role;

class RoleRepository extends BaseRepository
{
    public function __construct(Role $model)
    {
        $this->obj = $model;
    }

    public function  getData()
    {
        $validated = request()->validate([
            'nama' => 'nullable|array',
            'keterangan' => 'nullable|array',
            'status' => 'nullable|array',
            'created_at' => 'nullable|array',
            'created_at.*' => 'nullable|date_format:d-m-Y',
            'created_by' => 'nullable|array',
        ]);

        $query = $this->obj
            ->when($validated != [], function ($q) use ($validated) {
                filterData($q, $validated);
            });

        return $query;
    }
}
