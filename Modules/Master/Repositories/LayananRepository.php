<?php

namespace Modules\Master\Repositories;

use App\Repositories\BaseRepository;
use Modules\Master\Entities\Layanan;

class LayananRepository extends BaseRepository
{
    public function __construct(Layanan $model)
    {
        $this->obj = $model;
    }

    public function  getData()
    {
        $validated = request()->validate([
            'kode' => 'nullable|array',
            'nama' => 'nullable|array',
            'created_at' => 'nullable|array',
            'created_at.*' => 'nullable|date_format:d-m-Y',
            'created_by' => 'nullable|array',
        ]);

        $query = $this->obj
            ->when($validated != [], function ($q) use ($validated) {
                filterData($q, $validated);
            });

        return $query;
    }
}
