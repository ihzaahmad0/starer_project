<?php

namespace Modules\Master\Repositories;

use App\Repositories\BaseRepository;
use Modules\Master\Entities\Utilitas;

class UtilitasRepository extends BaseRepository
{
    public function __construct(Utilitas $model)
    {
        $this->obj = $model;
    }

    public function  getData()
    {
        $validated = request()->validate([
            'kode' => 'nullable|array',
            'nama' => 'nullable|array',
            'created_at' => 'nullable|array',
            'created_at.*' => 'nullable|date_format:d-m-Y',
            'satuan_id' => 'nullable|array',
            'created_by' => 'nullable|array',
        ]);

        $query = $this->obj
            ->with('satuan:id,nama')
            ->when($validated != [], function ($q) use ($validated) {
                filterData($q, $validated);
            })
            ->select($this->obj->getTable() . '.*');

        return $query;
    }
}
