<?php

namespace Modules\Master\Repositories;

use App\Repositories\BaseRepository;
use Modules\Master\Entities\Entitas;
use Illuminate\Support\Facades\DB;

class EntitasRepository extends BaseRepository
{
    public function __construct(Entitas $model)
    {
        $this->obj = $model;
    }

    public function  getData()
    {
        $validated = request()->validate([
            'nama' => 'nullable|array',
            'alamat' => 'nullable|array',
            'created_at' => 'nullable|array',
            'created_at.*' => 'nullable|date_format:d-m-Y',
            'created_by' => 'nullable|array',
            'deleted_at' => 'nullable|array',
        ]);

        $query = $this->obj
            ->when($validated != [], function ($q) use ($validated) {
                filterData($q, $validated);
            });

        return $query;
    }

    public function getDataKawasanIndustri()
    {
        $query = $this->getData();
        if (!auth()->user()->can('Super-Admin')) {
            $query->where('id', auth()->user()->entitas_id);
        }

        return $query;
    }

    public function storeFromRequest($data)
    {
        DB::beginTransaction();
        $obj = new Entitas();
        $obj->nama = $data['nama'];
        $obj->singkatan = $data['singkatan'];
        $obj->kode = $data['kode'];
        $obj->alamat = $data['alamat'];
        $obj->kota = $data['kota'];
        if (optional($data)['entitas_image']) {
            $entitas_image = $obj->upload_image($data['entitas_image'], 'entitas', 90);
            $obj->entitas_image = $entitas_image['full_path'];
        }
        $obj->save();
        DB::commit();

        return $obj;
    }

    public function updateFromRequest($id, $data)
    {
        DB::beginTransaction();
        $obj = $this->getById($id);
        $obj->nama = $data['nama'];
        $obj->singkatan = $data['singkatan'];
        $obj->alamat = $data['alamat'];
        $obj->kota = $data['kota'];
        if (optional($data)['entitas_image']) {
            $entitas_image = $obj->upload_image($data['entitas_image'], 'entitas', 90);
            $obj->entitas_image = $entitas_image['full_path'];
        }
        $obj->save();
        DB::commit();

        return $obj;
    }
}
