<?php

namespace Modules\Master\Entities;

use App\Models\BaseModel;
use Modules\KawasanIndustri\Entities\Supplier as KawasanIndistruSupplier;
use Modules\Tenant\Entities\Supplier as TenantSupplier;
use Modules\Tenant\Entities\Tenant;

class Layanan extends BaseModel
{
    protected $guarded = [];

    protected $table = "ref_layanan";
    public $check_on_delete_relations = ['kawasan_industri', 'tenant', 'supplier'];

    public function supplier()
    {
        return $this->hasMany(Supplier::class);
    }

    public function tenant()
    {
        return $this->belongsToMany(Tenant::class, TenantSupplier::getTableName())->wherePivot('deleted_at', null);
    }

    public function kawasan_industri()
    {
        return $this->belongsToMany(Entitas::class, KawasanIndistruSupplier::getTableName())->wherePivot('deleted_at', null);
    }
}
