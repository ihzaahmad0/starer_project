<?php

namespace Modules\Master\Entities;

use App\Models\BaseModel;
use Modules\KawasanIndustri\Entities\Karyawan as KawasanIndustriStatusKaryawan;
use Modules\KawasanIndustri\Entities\Pengurus as KawasanIndustriPengurus;
use Modules\Tenant\Entities\Karyawan as TenantStatusKaryawan;
use Modules\Tenant\Entities\Pengurus as TenantPengurusStatusKaryawan;
use Modules\Tenant\Entities\Tenant;

class StatusKaryawan extends BaseModel
{
    protected $guarded = [];

    protected $table = "ref_status_karyawan";
    public $check_on_delete_relations = ['kawasan_industri_karyawan', 'kawasan_industri_pengurus', 'tenant_karyawan', 'tenant_pengurus'];

    public function kawasan_industri_karyawan()
    {
        return $this->belongsToMany(Entitas::class, KawasanIndustriStatusKaryawan::getTableName())->wherePivot('deleted_at', null);
    }

    public function kawasan_industri_pengurus()
    {
        return $this->belongsToMany(Entitas::class, KawasanIndustriPengurus::getTableName())->wherePivot('deleted_at', null);
    }

    public function tenant_karyawan()
    {
        return $this->belongsToMany(Tenant::class, TenantStatusKaryawan::getTableName())->wherePivot('deleted_at', null);
    }

    public function tenant_pengurus()
    {
        return $this->belongsToMany(Tenant::class, TenantPengurusStatusKaryawan::getTableName())->wherePivot('deleted_at', null);
    }
}
