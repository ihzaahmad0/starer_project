<?php

namespace Modules\Master\Entities;

use App\Models\BaseModel;
use Modules\KawasanIndustri\Entities\Utilitas as KawasanIndustriUtilitas;
use Modules\Tenant\Entities\Tenant;
use Modules\Tenant\Entities\Utilitas as TenantUtilitas;

class Utilitas extends BaseModel
{
    protected $guarded = [];

    protected $table = "ref_utilitas";
    public $check_on_delete_relations = ['satuan', 'kawasan_industri', 'tenant'];

    public function satuan()
    {
        return $this->belongsTo(Satuan::class);
    }

    public function kawasan_industri()
    {
        return $this->belongsToMany(Entitas::class, KawasanIndustriUtilitas::getTableName())->wherePivot('deleted_at', null);
    }

    public function tenant()
    {
        return $this->belongsToMany(Tenant::class, TenantUtilitas::getTableName())->wherePivot('deleted_at', null);
    }
}
