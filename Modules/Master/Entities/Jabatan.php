<?php

namespace Modules\Master\Entities;

use App\Models\BaseModel;
use Modules\KawasanIndustri\Entities\Pengurus as KawasanIndustriPengurus;
use Modules\Tenant\Entities\Pengurus as TenantPengurus;
use Modules\Tenant\Entities\Tenant;

class Jabatan extends BaseModel
{
    protected $guarded = [];

    protected $table = "ref_jabatan";
    public $check_on_delete_relations = ['kawasan_industri', 'tenant'];

    public function kawasan_industri()
    {
        return $this->belongsToMany(Entitas::class, KawasanIndustriPengurus::getTableName())->wherePivot('deleted_at', null);
    }

    public function tenant()
    {
        return $this->belongsToMany(Tenant::class, TenantPengurus::getTableName())->wherePivot('deleted_at', null);
    }
}
