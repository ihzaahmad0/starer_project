<?php

namespace Modules\Master\Entities;

use App\Models\BaseModel;
use App\Models\User;
use Modules\KawasanIndustri\Entities\Karyawan;
use Modules\KawasanIndustri\Entities\Pengurus;
use Modules\KawasanIndustri\Entities\Supplier;
use Modules\KawasanIndustri\Entities\Utilitas;
use Modules\Tenant\Entities\Tenant;
use App\Traits\HasImage;

class Entitas extends BaseModel
{
    use HasImage;
    protected $guarded = [];

    protected $table = "ref_entitas";
    public $check_on_delete_relations = ['user', 'kawasan_industri_utilitas', 'kawasan_industri_karyawan', 'kawasan_industri_supplier', 'kawasan_industri_pengurus', 'tenant'];

    public function user()
    {
        return $this->hasMany(User::class);
    }

    public function kawasan_industri_utilitas()
    {
        return $this->belongsToMany(Entitas::class, Utilitas::getTableName())->wherePivot('deleted_at', null);
    }

    public function kawasan_industri_karyawan()
    {
        return $this->belongsToMany(Entitas::class, Karyawan::getTableName())->wherePivot('deleted_at', null);
    }

    public function kawasan_industri_pengurus()
    {
        return $this->belongsToMany(Entitas::class, Pengurus::getTableName())->wherePivot('deleted_at', null);
    }

    public function kawasan_industri_supplier()
    {
        return $this->belongsToMany(Entitas::class, Supplier::getTableName())->wherePivot('deleted_at', null);
    }

    public function tenant()
    {
        return $this->hasMany(Tenant::class);
    }
}
