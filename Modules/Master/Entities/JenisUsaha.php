<?php

namespace Modules\Master\Entities;

use App\Models\BaseModel;
use Modules\Tenant\Entities\Tenant;

class JenisUsaha extends BaseModel
{
    protected $guarded = [];
    protected $table = "ref_jenis_usaha";
    public $check_on_delete_relations = ['tenant'];

    public function tenant()
    {
        return $this->hasMany(Tenant::class);
    }
}
