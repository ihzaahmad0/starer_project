<?php

namespace Modules\Master\Entities;

use App\Models\BaseModel;

class Satuan extends BaseModel
{
    protected $guarded = [];

    protected $table = "ref_satuan";
    public $check_on_delete_relations = ['utilitas'];

    public function utilitas()
    {
        return $this->hasMany(Utilitas::class);
    }
}
