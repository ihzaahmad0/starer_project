<?php

namespace Modules\Master\Entities;

use App\Models\BaseModel;
use Modules\KawasanIndustri\Entities\Supplier as KawasanIndustriSupplier;
use Modules\Tenant\Entities\Supplier as TenantSupplier;
use Modules\Tenant\Entities\Tenant;

class Supplier extends BaseModel
{
    protected $guarded = [];

    protected $table = "ref_supplier";
    public $check_on_delete_relations = ['layanan', 'kawasan_industri', 'tenant'];

    public function layanan()
    {
        return $this->belongsTo(Layanan::class);
    }

    public function kawasan_industri()
    {
        return $this->belongsToMany(Entitas::class, KawasanIndustriSupplier::getTableName())->wherePivot('deleted_at', null);
    }

    public function tenant()
    {
        return $this->belongsToMany(Tenant::class, TenantSupplier::getTableName())->wherePivot('deleted_at', null);
    }
}
