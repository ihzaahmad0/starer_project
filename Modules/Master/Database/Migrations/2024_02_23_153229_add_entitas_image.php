<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Master\Entities\Entitas;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table(Entitas::getTableName(), function (Blueprint $table) {
            $table->string('entitas_image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table(Entitas::getTableName(), function (Blueprint $table) {
            $table->dropColumn('entitas_image');
        });
    }
};
