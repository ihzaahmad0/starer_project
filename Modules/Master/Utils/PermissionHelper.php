<?php

namespace Modules\Master\Utils;

class PermissionHelper
{
    /**
     * Permission rule:
     *
     * 1. you can set to string, but the action will use the default actions
     * 2. you can set to array with 3 following keys:
     *  2.a. name (mandatory) => The name of the permission, this will stored in database. Use this for middleware
     *  2.b. alias (optional) => The display name of permission in permission page
     *  2.c. actions (optional) => The actions for this permission. If not set, will use default actions
     *  2.d. description (optional) => The description of this permission
     */
    const PERMISSIONS = [
        'Master' => [
            [
                'name' => 'master.utilitas',
                'alias' => 'Master Utilitas',
            ],
            [
                'name' => 'master.jenis_usaha',
                'alias' => 'Master Jenis Usaha',
            ],
            [
                'name' => 'master.status_karyawan',
                'alias' => 'Master Status Karyawan',
            ],
            [
                'name' => 'master.satuan',
                'alias' => 'Master Satuan',
            ],
            [
                'name' => 'master.jabatan',
                'alias' => 'Master Jabatan',
            ],
            [
                'name' => 'master.identitas',
                'alias' => 'Master Identitas',
            ],
            [
                'name' => 'master.supplier',
                'alias' => 'Master Supplier',
            ],
            [
                'name' => 'master.layanan',
                'alias' => 'Master Layanan',
            ],
            [
                'name' => 'master.entitas',
                'alias' => 'Master Entitas',
            ]
        ],
    ];
}
