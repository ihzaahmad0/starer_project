<div class="card">
    <form action="{{ route($route . 'store') }}" method="POST" autocomplete="off" id="main-form" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
            @include($view . 'components.detail')
            <x-forms.input-image-with-preview img-preview-id="entitas_image" label="Foto enttias" 
            is-required="FALSE" input-name="entitas_image" :img-src="asset('assets/img/avatars/default.png')" class="mt-3" />
        </div>
        <div class="card-footer text-muted text-center">
            <button class="btn btn-primary"><i class="fas fa-save"></i> Tambah</button>
        </div>
    </form>
</div>
