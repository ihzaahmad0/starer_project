<div class="card">
    <form action="{{ route($route . 'update', ['id' => $data['obj']->id]) }}" method="POST" autocomplete="off"
        id="main-form" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
            @include($view . 'components.detail')
            <x-forms.input-image-with-preview img-preview-id="entitas_image" label="Foto entitas"
                            is-required="FALSE" input-name="entitas_image" :img-src="$data['obj']->image('entitas_image')
                                ? $data['obj']->image('entitas_image')
                                : asset('assets/img/avatars/default.png')" class="mt-3" />
        </div>
        <div class="card-footer text-muted text-center">
            <button class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
        </div>
    </form>
</div>

<script>
    // disable input that have name = kode
    $("input[name='kode']").prop('disabled', true);
</script>