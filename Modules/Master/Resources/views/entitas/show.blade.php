<div class="card">
    <fieldset disabled>
        <div class="card-body">
        <img src="{{ asset('storage/'. $data['obj']->entitas_image) }}" style="height: 100px;width:100px;">
            @include($view . 'components.detail')
        </div>
    </fieldset>
</div>
