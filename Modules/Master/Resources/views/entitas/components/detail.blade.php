<div class="form-group my-2">
    <label for="nama">Nama <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama..."
        value="{{ optional($data)['obj'] ? $data['obj']->nama : '' }}" required>
</div>
<div class="form-group my-2">
    <label for="singkatan">Singkatan <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="singkatan" name="singkatan" placeholder="Singkatan..."
        value="{{ optional($data)['obj'] ? $data['obj']->singkatan : '' }}" required>
</div>
<div class="form-group my-2">
    <label for="kode">Kode <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="kode" name="kode" placeholder="Kode..."
        value="{{ optional($data)['obj'] ? $data['obj']->kode : '' }}" required>
</div>
<div class="form-group my-2">
    <label for="alamat">Alamat <span class="text-danger">*</span></label>
    <textarea class="form-control" id="alamat" name="alamat" placeholder="Alamat..." required>{{ optional($data)['obj'] ? $data['obj']->alamat : '' }}</textarea>
</div>
<div class="form-group my-2">
    <label for="kota">Kota <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="kota" name="kota" placeholder="Kota..."
        value="{{ optional($data)['obj'] ? $data['obj']->kota : '' }}" required>
</div>