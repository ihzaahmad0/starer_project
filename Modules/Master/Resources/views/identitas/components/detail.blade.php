<div class="form-group my-2">
    <label for="kode">Kode Identitas <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="kode" name="kode" placeholder="Kode Identitas..."
        value="{{ optional($data)['obj'] ? $data['obj']->kode : '' }}" required>
</div>
<div class="form-group my-2">
    <label for="nama">Nama Identitas <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="nama" name="nama" placeholder="Kode Identitas..."
        value="{{ optional($data)['obj'] ? $data['obj']->nama : '' }}" required>
</div>
