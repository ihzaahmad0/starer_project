<div class="form-group my-2">
    <label for="kode">Kode Supplier <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="kode" name="kode" placeholder="Kode Supplier..."
        value="{{ optional($data)['obj'] ? $data['obj']->kode : '' }}" required>
</div>
<div class="form-group my-2">
    <label for="singkatan">Singkatan Supplier <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="singkatan" name="singkatan" placeholder="Singkatan Supplier..."
        value="{{ optional($data)['obj'] ? $data['obj']->singkatan : '' }}" required>
</div>
<div class="form-group my-2">
    <label for="nama">Nama Supplier <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="nama" name="nama" placeholder="Kode Supplier..."
        value="{{ optional($data)['obj'] ? $data['obj']->nama : '' }}" required>
</div>
<div class="form-group my-2">
    <label for="layanan_id">Layanan <span class="text-danger">*</span></label>
    <select class="form-control datatable-filter-select-ajax" id="layanan_id" name="layanan_id" required>
        <option value="" selected disabled>-- Pilih --</option>
        @if (optional($data)['obj'])
            @php
                $layanan = optional(optional($data)['obj'])->layanan;
            @endphp
            <option value="{{ $layanan->id }}" selected>
                {{ $layanan->nama }}
            </option>
        @endif
    </select>
</div>
