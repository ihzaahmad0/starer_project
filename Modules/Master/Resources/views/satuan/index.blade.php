@extends('layouts.master')

@section('page_title', 'Master')
@section('page_sub_title', 'Satuan')

@section('breadcrumb')
    @php
        $breadcrumbs = ['Master', ['Satuan', route($route . 'index')]];
    @endphp
    @include('layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex">
                    <div class="head-label text-center">
                        <h5 class="card-title mb-0">List Satuan</h5>
                    </div>
                    <div class="card-tools ms-auto">
                        @can('create ' . $permission)
                            <a class="btn btn-primary btn-sm btn-ajax-open-model-lg" href="{{ route($route . 'create') }}"
                                data-bs-toggle="tooltip" data-bs-title="Tambah Data" data-title="Tambah Data"><i
                                    class="fa fa-plus" aria-hidden="true"></i>
                                Tambah</a>
                        @endcan
                    </div>
                    <!-- /.card-tools -->
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered" id="main-table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Satuan</th>
                                <th>Nama Satuan</th>
                                <th>Created By</th>
                                <th>Created At</th>
                                <th style="width: 7%">Action</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th>
                                    @include('layouts.datatables.filter.input', [
                                        'name' => 'kode[il]',
                                        'placeholder' => 'kode...',
                                    ])
                                </th>
                                <th>
                                    @include('layouts.datatables.filter.input', [
                                        'name' => 'nama[il]',
                                        'placeholder' => 'nama...',
                                    ])
                                </th>
                                <th>
                                    @include('layouts.datatables.filter.select_ajax', [
                                        'name' => 'created_by[e]',
                                        'url' => route('user_config.select2.user'),
                                    ])
                                </th>
                                <th>
                                    @include('layouts.datatables.filter.datepicker', [
                                        'name' => 'created_at[e]',
                                    ])
                                </th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('layouts.data_tables.basic_data_tables')
@include('layouts.modals.modal-lg')

@push('scripts')
    <script src="{{ asset('assets/js/datatables/init_datatable.js') }}"></script>
    <script src="{{ asset('assets/js/fetch_api/fetchPost.js') }}"></script>
    <script src="{{ asset('assets/js/utils/btn-delete-item-datatable.js') }}"></script>

    <script>
        $main_datatable = init_serverside_datatable(
            '#main-table',
            "{{ route($route . 'datatable') }}",
            [{
                    data: 'kode',
                },
                {
                    data: 'nama',
                },
                {
                    data: 'created_by',
                    sortable: false
                },
                {
                    data: 'created_at',
                    sortable: false
                },
                {
                    data: 'action',
                    sortable: false,
                    className: "text-center"
                }
            ]
        );

        $(document).on('submit', "#main-form", function() {
            event.preventDefault();
            loader.show();
            fetchPost($(this).attr("action"), new FormData(this))
                .then((data) => {
                    Toast.success(data.message);
                    $main_datatable.draw();
                    // hide closest modal
                    $(this).closest(".modal").modal("hide");
                })
                .finally(() => {
                    loader.hide();
                });
        });
    </script>
@endpush
