<div class="card">
    <form action="{{ route($route . 'store') }}" method="POST" autocomplete="off" id="main-form">
        @csrf
        <div class="card-body">
            @include($view . 'components.detail')
        </div>
        <div class="card-footer text-muted text-center">
            <button class="btn btn-primary"><i class="fas fa-save"></i> Tambah</button>
        </div>
    </form>
</div>
