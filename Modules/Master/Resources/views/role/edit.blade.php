<div class="card">
    <form action="{{ route($route . 'update', ['id' => $data['obj']->id]) }}" method="POST" autocomplete="off"
        id="main-form">
        @csrf
        <div class="card-body">
            @include($view . 'components.detail')
        </div>
        <div class="card-footer text-muted text-center">
            <button class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
        </div>
    </form>
</div>
