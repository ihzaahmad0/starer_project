<div class="form-group my-2">
    <label for="nama">Role <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="nama" name="nama" placeholder="Role..."
        value="{{ optional($data)['obj'] ? $data['obj']->nama : '' }}" required>
</div>
<div class="form-group my-2">
    <label for="status">Status <span class="text-danger">*</span></label>
    <select class="form-control" id="status" name="status" required>
        <option value="Active">Active</option>
        <option value="Inactive">Inactive</option>
    </select>
</div>
