<div class="card">
    <fieldset disabled>
        <div class="card-body">
            @include($view . 'components.detail')
        </div>
    </fieldset>
</div>
