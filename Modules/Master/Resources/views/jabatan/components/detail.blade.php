<div class="form-group my-2">
    <label for="kode">Kode Jabatan <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="kode" name="kode" placeholder="Kode Jabatan..."
        value="{{ optional($data)['obj'] ? $data['obj']->kode : '' }}" required>
</div>
<div class="form-group my-2">
    <label for="nama">Nama Jabatan <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="nama" name="nama" placeholder="Kode Jabatan..."
        value="{{ optional($data)['obj'] ? $data['obj']->nama : '' }}" required>
</div>
