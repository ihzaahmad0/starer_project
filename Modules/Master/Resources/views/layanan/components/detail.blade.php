<div class="form-group my-2">
    <label for="kode">Kode Layanan <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="kode" name="kode" placeholder="Kode Layanan..."
        value="{{ optional($data)['obj'] ? $data['obj']->kode : '' }}" required>
</div>
<div class="form-group my-2">
    <label for="nama">Nama Layanan <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="nama" name="nama" placeholder="Kode Layanan..."
        value="{{ optional($data)['obj'] ? $data['obj']->nama : '' }}" required>
</div>
