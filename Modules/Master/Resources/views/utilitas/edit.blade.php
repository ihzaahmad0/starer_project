<div class="card">
    <form action="{{ route($route . 'update', ['id' => $data['obj']->id]) }}" method="POST" autocomplete="off" id="main-form">
        @csrf
        <div class="card-body">
            @include($view . 'components.detail')
        </div>
        <div class="card-footer text-muted text-center">
            <button class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
        </div>
    </form>
</div>
<script src="{{ asset('assets/js/select2/ajax_data_pagination.js') }}"></script>
<script>
    $(document).ready(function() {
        ajax_select2_pagination('#satuan_id', "{{ route('master.select2.satuan') }}", {
            dropdownParent: $("#main-form")
        });
        // disable input that have name = kode
        $("input[name='kode']").prop('disabled', true);
    });
</script>
