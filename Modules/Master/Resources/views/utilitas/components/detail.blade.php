<div class="form-group my-2">
    <label for="kode">Kode Utilitas <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="kode" name="kode" placeholder="Kode Utilitas..."
        value="{{ optional($data)['obj'] ? $data['obj']->kode : '' }}" required>
</div>
<div class="form-group my-2">
    <label for="nama">Nama Utilitas <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="nama" name="nama" placeholder="Kode Utilitas..."
        value="{{ optional($data)['obj'] ? $data['obj']->nama : '' }}" required>
</div>
<div class="form-group my-2">
    <label for="satuan_id">Satuan <span class="text-danger">*</span></label>
    <select class="form-control datatable-filter-select-ajax" id="satuan_id" name="satuan_id" required>
        <option value="" selected disabled>-- Pilih --</option>
        @if (optional($data)['obj'])
            @php
                $satuan = optional(optional($data)['obj'])->satuan;
            @endphp
            <option value="{{ $satuan->id }}" selected>
                {{ $satuan->nama }}
            </option>
        @endif
    </select>
</div>
