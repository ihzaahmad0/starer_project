<div class="card">
    <form action="{{ route($route . 'store') }}" method="POST" autocomplete="off" id="main-form">
        @csrf
        <div class="card-body">
            @include($view . 'components.detail')
        </div>
        <div class="card-footer text-muted text-center">
            <button class="btn btn-primary"><i class="fas fa-save"></i> Tambah</button>
        </div>
    </form>
</div>
<script src="{{ asset('assets/js/select2/ajax_data_pagination.js') }}"></script>
<script>
    $(document).ready(function() {
        ajax_select2_pagination('#satuan_id', "{{ route('master.select2.satuan') }}",{
            dropdownParent : $("#main-form")
        });
    });
</script>
