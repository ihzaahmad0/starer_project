<?php

use Illuminate\Support\Facades\Route;
use Modules\Master\Entities\Layanan;
use Modules\Master\Entities\Satuan;

Route::middleware(['auth'])->prefix('master/select2')->name('master.select2.')->group(function () {
    Route::match(['get', 'post'], 'satuan', function () {
        return select2_route(Satuan::class, 'id', 'nama');
    })->name('satuan');

    Route::match(['get', 'post'], 'layanan', function () {
        return select2_route(Layanan::class, 'id', 'kode');
    })->name('layanan');
});
