<?php

use Illuminate\Support\Facades\Route;
use Modules\Master\Entities\Layanan;

Route::middleware(['auth'])->prefix('master/select2/layanan')->name('master.select2.layanan.')->group(function () {
    Route::match(['get', 'post'], 'kode', function () {
        return select2_route(Layanan::class, 'id', 'kode');
    })->name('kode');

    Route::match(['get', 'post'], 'combine', function () {
        return select2_route(Layanan::class, 'id', ['kode', 'nama']);
    })->name('combine');
});
