<?php

use Illuminate\Support\Facades\Route;
use Modules\Master\Entities\Identitas;

Route::middleware(['auth'])->prefix('master/select2/identitas')->name('master.select2.identitas.')->group(function () {
    Route::match(['get', 'post'], '', function () {
        return select2_route(Identitas::class, 'id', 'nama');
    });

    Route::match(['get', 'post'], 'kode', function () {
        return select2_route(Identitas::class, 'id', 'kode');
    })->name('kode');

    Route::match(['get', 'post'], 'combine', function () {
        return select2_route(Identitas::class, 'id', ['kode', 'nama']);
    })->name('combine');
});
