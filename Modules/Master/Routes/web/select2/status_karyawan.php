<?php

use Illuminate\Support\Facades\Route;
use Modules\Master\Entities\StatusKaryawan;

Route::middleware(['auth'])->prefix('master/select2/status_karyawan')->name('master.select2.status_karyawan.')->group(function () {
    Route::match(['get', 'post'], '', function () {
        return select2_route(StatusKaryawan::class, 'id', 'nama');
    });

    Route::match(['get', 'post'], 'kode', function () {
        return select2_route(StatusKaryawan::class, 'id', 'kode');
    })->name('kode');

    Route::match(['get', 'post'], 'combine', function () {
        return select2_route(StatusKaryawan::class, 'id', ['kode', 'nama']);
    })->name('combine');
});
