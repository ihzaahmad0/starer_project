<?php

use Illuminate\Support\Facades\Route;
use Modules\Master\Entities\Utilitas;

Route::middleware(['auth'])->prefix('master/select2/utilitas')->name('master.select2.utilitas.')->group(function () {
    Route::match(['get', 'post'], '', function () {
        return select2_route(Utilitas::class, 'id', 'nama');
    });

    Route::match(['get', 'post'], 'kode', function () {
        return select2_route(Utilitas::class, 'id', 'kode');
    })->name('kode');

    Route::match(['get', 'post'], 'combine', function () {
        return select2_route(Utilitas::class, 'id', ['kode', 'nama']);
    })->name('combine');
});
