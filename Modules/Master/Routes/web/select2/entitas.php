<?php

use Illuminate\Support\Facades\Route;
use Modules\Master\Entities\Entitas;

Route::middleware(['auth'])->prefix('master/select2/entitas')->name('master.select2.entitas.')->group(function () {
    Route::match(['get', 'post'], '', function () {
        return select2_route(Entitas::class, 'id', 'nama');
    });

    Route::match(['get', 'post'], 'kode', function () {
        return select2_route(Entitas::class, 'id', 'kode');
    })->name('kode');

    Route::match(['get', 'post'], 'combine', function () {
        return select2_route(Entitas::class, 'id', ['kode', 'nama']);
    })->name('combine');
});
