<?php

use Illuminate\Support\Facades\Route;
use Modules\Master\Entities\Jabatan;

Route::middleware(['auth'])->prefix('master/select2/jabatan')->name('master.select2.jabatan.')->group(function () {
    Route::match(['get', 'post'], '', function () {
        return select2_route(Jabatan::class, 'id', 'nama');
    });

    Route::match(['get', 'post'], 'kode', function () {
        return select2_route(Jabatan::class, 'id', 'kode');
    })->name('kode');

    Route::match(['get', 'post'], 'combine', function () {
        return select2_route(Jabatan::class, 'id', ['kode', 'nama']);
    })->name('combine');
});
