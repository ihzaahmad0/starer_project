<?php

use Illuminate\Support\Facades\Route;
use Modules\Master\Entities\JenisUsaha;

Route::middleware(['auth'])->prefix('master/select2/jenis_usaha')->name('master.select2.jenis_usaha.')->group(function () {
    Route::match(['get', 'post'], '', function () {
        return select2_route(JenisUsaha::class, 'id', 'nama');
    });

    Route::match(['get', 'post'], 'kode', function () {
        return select2_route(JenisUsaha::class, 'id', 'kode');
    })->name('kode');

    Route::match(['get', 'post'], 'combine', function () {
        return select2_route(JenisUsaha::class, 'id', ['kode', 'nama']);
    })->name('combine');
});
