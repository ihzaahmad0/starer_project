<?php

use Illuminate\Support\Facades\Route;
use Modules\Master\Entities\Supplier;

Route::middleware(['auth'])->prefix('master/select2/supplier')->name('master.select2.supplier.')->group(function () {
    Route::match(['get', 'post'], '', function () {
        return select2_route(Supplier::class, 'id', 'nama');
    });

    Route::match(['get', 'post'], 'kode', function () {
        return select2_route(Supplier::class, 'id', 'kode');
    })->name('kode');

    Route::match(['get', 'post'], 'combine', function () {
        return select2_route(Supplier::class, 'id', ['kode', 'nama']);
    })->name('combine');
});
