<?php

use Illuminate\Support\Facades\Route;
use Modules\Master\Http\Controllers\Web\SupplierController;

$permission = 'master.supplier';
Route::middleware(['auth'])->prefix('master/supplier')->name('master.supplier.')->group(function () use ($permission) {
    Route::middleware(['permission:view ' . $permission])->group(function () {
        Route::get('', [SupplierController::class, 'index'])->name('index');
        Route::get('datatable', [SupplierController::class, 'datatable'])->name('datatable');
        Route::get('show/{id}', [SupplierController::class, 'show'])->name('show');
    });

    Route::middleware(['permission:create ' . $permission])->group(function () {
        Route::get('create', [SupplierController::class, 'create'])->name('create');
        Route::post('store', [SupplierController::class, 'store'])->name('store');
    });

    Route::middleware(['permission:update ' . $permission])->group(function () {
        Route::get('edit/{id}', [SupplierController::class, 'edit'])->name('edit');
        Route::post('update/{id}', [SupplierController::class, 'update'])->name('update');
    });

    Route::middleware(['permission:delete ' . $permission])->group(function () {
        Route::delete('delete/{id}', [SupplierController::class, 'delete'])->name('delete');
    });
});
