<?php

use Illuminate\Support\Facades\Route;
use Modules\Master\Http\Controllers\Web\SatuanController;

$permission = 'master.satuan';
Route::middleware(['auth'])->prefix('master/satuan')->name('master.satuan.')->group(function () use ($permission) {
    Route::middleware(['permission:view ' . $permission])->group(function () {
        Route::get('', [SatuanController::class, 'index'])->name('index');
        Route::get('datatable', [SatuanController::class, 'datatable'])->name('datatable');
        Route::get('show/{id}', [SatuanController::class, 'show'])->name('show');
    });

    Route::middleware(['permission:create ' . $permission])->group(function () {
        Route::get('create', [SatuanController::class, 'create'])->name('create');
        Route::post('store', [SatuanController::class, 'store'])->name('store');
    });

    Route::middleware(['permission:update ' . $permission])->group(function () {
        Route::get('edit/{id}', [SatuanController::class, 'edit'])->name('edit');
        Route::post('update/{id}', [SatuanController::class, 'update'])->name('update');
    });

    Route::middleware(['permission:delete ' . $permission])->group(function () {
        Route::delete('delete/{id}', [SatuanController::class, 'delete'])->name('delete');
    });
});
