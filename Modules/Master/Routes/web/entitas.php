<?php

use Illuminate\Support\Facades\Route;
use Modules\Master\Http\Controllers\Web\EntitasController;

$permission = 'master.entitas';
Route::middleware(['auth'])->prefix('master/entitas')->name('master.entitas.')->group(function () use ($permission) {
    Route::middleware(['permission:view ' . $permission])->group(function () {
        Route::get('', [EntitasController::class, 'index'])->name('index');
        Route::get('datatable', [EntitasController::class, 'datatable'])->name('datatable');
        Route::get('show/{id}', [EntitasController::class, 'show'])->name('show');
    });

    Route::middleware(['permission:create ' . $permission])->group(function () {
        Route::get('create', [EntitasController::class, 'create'])->name('create');
        Route::post('store', [EntitasController::class, 'store'])->name('store');
    });

    Route::middleware(['permission:update ' . $permission])->group(function () {
        Route::get('edit/{id}', [EntitasController::class, 'edit'])->name('edit');
        Route::post('update/{id}', [EntitasController::class, 'update'])->name('update');
    });

    Route::middleware(['permission:delete ' . $permission])->group(function () {
        Route::delete('delete/{id}', [EntitasController::class, 'delete'])->name('delete');
    });
});
