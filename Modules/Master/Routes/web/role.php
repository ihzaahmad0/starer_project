<?php

use Illuminate\Support\Facades\Route;
use Modules\Master\Http\Controllers\Web\RoleController;

$permission = 'master.role';
Route::middleware(['auth'])->prefix('master/role')->name('master.role.')->group(function () use ($permission) {
    Route::middleware(['permission:view ' . $permission])->group(function () {
        Route::get('', [RoleController::class, 'index'])->name('index');
        Route::get('datatable', [RoleController::class, 'datatable'])->name('datatable');
        Route::get('show/{id}', [RoleController::class, 'show'])->name('show');
    });

    Route::middleware(['permission:create ' . $permission])->group(function () {
        Route::get('create', [RoleController::class, 'create'])->name('create');
        Route::post('store', [RoleController::class, 'store'])->name('store');
    });

    Route::middleware(['permission:update ' . $permission])->group(function () {
        Route::get('edit/{id}', [RoleController::class, 'edit'])->name('edit');
        Route::post('update/{id}', [RoleController::class, 'update'])->name('update');
    });

    Route::middleware(['permission:delete ' . $permission])->group(function () {
        Route::delete('delete/{id}', [RoleController::class, 'delete'])->name('delete');
    });
});
