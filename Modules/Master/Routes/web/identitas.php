<?php

use Illuminate\Support\Facades\Route;
use Modules\Master\Http\Controllers\Web\IdentitasController;

$permission = 'master.identitas';
Route::middleware(['auth'])->prefix('master/identitas')->name('master.identitas.')->group(function () use ($permission) {
    Route::middleware(['permission:view ' . $permission])->group(function () {
        Route::get('', [IdentitasController::class, 'index'])->name('index');
        Route::get('datatable', [IdentitasController::class, 'datatable'])->name('datatable');
        Route::get('show/{id}', [IdentitasController::class, 'show'])->name('show');
    });

    Route::middleware(['permission:create ' . $permission])->group(function () {
        Route::get('create', [IdentitasController::class, 'create'])->name('create');
        Route::post('store', [IdentitasController::class, 'store'])->name('store');
    });

    Route::middleware(['permission:update ' . $permission])->group(function () {
        Route::get('edit/{id}', [IdentitasController::class, 'edit'])->name('edit');
        Route::post('update/{id}', [IdentitasController::class, 'update'])->name('update');
    });

    Route::middleware(['permission:delete ' . $permission])->group(function () {
        Route::delete('delete/{id}', [IdentitasController::class, 'delete'])->name('delete');
    });
});
