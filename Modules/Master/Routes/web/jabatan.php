<?php

use Illuminate\Support\Facades\Route;
use Modules\Master\Http\Controllers\Web\JabatanController;

$permission = 'master.jabatan';
Route::middleware(['auth'])->prefix('master/jabatan')->name('master.jabatan.')->group(function () use ($permission) {
    Route::middleware(['permission:view ' . $permission])->group(function () {
        Route::get('', [JabatanController::class, 'index'])->name('index');
        Route::get('datatable', [JabatanController::class, 'datatable'])->name('datatable');
        Route::get('show/{id}', [JabatanController::class, 'show'])->name('show');
    });

    Route::middleware(['permission:create ' . $permission])->group(function () {
        Route::get('create', [JabatanController::class, 'create'])->name('create');
        Route::post('store', [JabatanController::class, 'store'])->name('store');
    });

    Route::middleware(['permission:update ' . $permission])->group(function () {
        Route::get('edit/{id}', [JabatanController::class, 'edit'])->name('edit');
        Route::post('update/{id}', [JabatanController::class, 'update'])->name('update');
    });

    Route::middleware(['permission:delete ' . $permission])->group(function () {
        Route::delete('delete/{id}', [JabatanController::class, 'delete'])->name('delete');
    });
});
