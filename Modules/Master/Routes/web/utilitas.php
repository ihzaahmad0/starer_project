<?php

use Illuminate\Support\Facades\Route;
use Modules\Master\Http\Controllers\Web\UtilitasController;

$permission = 'master.utilitas';
Route::middleware(['auth'])->prefix('master/utilitas')->name('master.utilitas.')->group(function () use ($permission) {
    Route::middleware(['permission:view ' . $permission])->group(function () {
        Route::get('', [UtilitasController::class, 'index'])->name('index');
        Route::get('datatable', [UtilitasController::class, 'datatable'])->name('datatable');
        Route::get('show/{id}', [UtilitasController::class, 'show'])->name('show');
    });

    Route::middleware(['permission:create ' . $permission])->group(function () {
        Route::get('create', [UtilitasController::class, 'create'])->name('create');
        Route::post('store', [UtilitasController::class, 'store'])->name('store');
    });

    Route::middleware(['permission:update ' . $permission])->group(function () {
        Route::get('edit/{id}', [UtilitasController::class, 'edit'])->name('edit');
        Route::post('update/{id}', [UtilitasController::class, 'update'])->name('update');
    });

    Route::middleware(['permission:delete ' . $permission])->group(function () {
        Route::delete('delete/{id}', [UtilitasController::class, 'delete'])->name('delete');
    });
});
