<?php

use Illuminate\Support\Facades\Route;
use Modules\Master\Http\Controllers\Web\StatusKaryawanController;

$permission = 'master.status_karyawan';
Route::middleware(['auth'])->prefix('master/status_karyawan')->name('master.status_karyawan.')->group(function () use ($permission) {
    Route::middleware(['permission:view ' . $permission])->group(function () {
        Route::get('', [StatusKaryawanController::class, 'index'])->name('index');
        Route::get('datatable', [StatusKaryawanController::class, 'datatable'])->name('datatable');
        Route::get('show/{id}', [StatusKaryawanController::class, 'show'])->name('show');
    });

    Route::middleware(['permission:create ' . $permission])->group(function () {
        Route::get('create', [StatusKaryawanController::class, 'create'])->name('create');
        Route::post('store', [StatusKaryawanController::class, 'store'])->name('store');
    });

    Route::middleware(['permission:update ' . $permission])->group(function () {
        Route::get('edit/{id}', [StatusKaryawanController::class, 'edit'])->name('edit');
        Route::post('update/{id}', [StatusKaryawanController::class, 'update'])->name('update');
    });

    Route::middleware(['permission:delete ' . $permission])->group(function () {
        Route::delete('delete/{id}', [StatusKaryawanController::class, 'delete'])->name('delete');
    });
});
