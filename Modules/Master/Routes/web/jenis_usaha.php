<?php

use Illuminate\Support\Facades\Route;
use Modules\Master\Http\Controllers\Web\JenisUsahaController;

$permission = 'master.jenis_usaha';
Route::middleware(['auth'])->prefix('master/jenis_usaha')->name('master.jenis_usaha.')->group(function () use ($permission) {
    Route::middleware(['permission:view ' . $permission])->group(function () {
        Route::get('', [JenisUsahaController::class, 'index'])->name('index');
        Route::get('datatable', [JenisUsahaController::class, 'datatable'])->name('datatable');
        Route::get('show/{id}', [JenisUsahaController::class, 'show'])->name('show');
    });

    Route::middleware(['permission:create ' . $permission])->group(function () {
        Route::get('create', [JenisUsahaController::class, 'create'])->name('create');
        Route::post('store', [JenisUsahaController::class, 'store'])->name('store');
    });

    Route::middleware(['permission:update ' . $permission])->group(function () {
        Route::get('edit/{id}', [JenisUsahaController::class, 'edit'])->name('edit');
        Route::post('update/{id}', [JenisUsahaController::class, 'update'])->name('update');
    });

    Route::middleware(['permission:delete ' . $permission])->group(function () {
        Route::delete('delete/{id}', [JenisUsahaController::class, 'delete'])->name('delete');
    });
});
