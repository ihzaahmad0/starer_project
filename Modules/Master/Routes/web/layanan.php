<?php

use Illuminate\Support\Facades\Route;
use Modules\Master\Http\Controllers\Web\LayananController;

$permission = 'master.layanan';
Route::middleware(['auth'])->prefix('master/layanan')->name('master.layanan.')->group(function () use ($permission) {
    Route::middleware(['permission:view ' . $permission])->group(function () {
        Route::get('', [LayananController::class, 'index'])->name('index');
        Route::get('datatable', [LayananController::class, 'datatable'])->name('datatable');
        Route::get('show/{id}', [LayananController::class, 'show'])->name('show');
    });

    Route::middleware(['permission:create ' . $permission])->group(function () {
        Route::get('create', [LayananController::class, 'create'])->name('create');
        Route::post('store', [LayananController::class, 'store'])->name('store');
    });

    Route::middleware(['permission:update ' . $permission])->group(function () {
        Route::get('edit/{id}', [LayananController::class, 'edit'])->name('edit');
        Route::post('update/{id}', [LayananController::class, 'update'])->name('update');
    });

    Route::middleware(['permission:delete ' . $permission])->group(function () {
        Route::delete('delete/{id}', [LayananController::class, 'delete'])->name('delete');
    });
});
