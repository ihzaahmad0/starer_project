<?php

namespace Modules\AuditTrail\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class AuditTrail extends Model
{
    protected $fillable = [
        'user_id',
        'action',
        'ip',
        'level',
        'old_data',
        'new_data'
    ];

    protected $casts = [
        'old_data' => 'array',
        'new_data' => 'array'
    ];

    public const ACTIONS = [
        'login',
        'logout',
        'access',
        'create',
        'read',
        'update',
        'delete',
    ];

    public const LEVEL = [
        1 => 1, 2 => 2, 3 => 3
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function actionRaw()
    {
        $maps = [
            'login' => 'success',
            'logout' => 'danger',
            'access' => 'info',
            'create' => 'success',
            'update' => 'warning',
            'delete' => 'danger',
            //primary, info, success, warning, danger, secondary, light, dark
        ];
        $action = '<span class="badge badge-' . (array_key_exists($this->action, $maps) ? $maps[$this->action] : 'primary') . ' text-uppercase">
                            ' . $this->action . '
                        </span>';
        return $action;
    }
}
