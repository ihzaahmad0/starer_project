<?php

namespace Modules\AuditTrail\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuditTrailAccessPage
{
    /**
     * Handle an incoming request.
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->check()) {
            auth()->user()->storeLog(2, 'Access Modul ' . $request->route()->getName());
        }

        return $next($request);
    }
}
