<?php

namespace Modules\AuditTrail\Http\Controllers;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\AuditTrail\Entities\AuditTrail;

class AuditTrailController extends BaseController
{

    public function __construct()
    {
        $this->route = 'audit-trail.';
        $this->view = 'audittrail::';
        $this->permission = 'audit_trail';
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data['level'] = AuditTrail::LEVEL;
        return $this->view('index', $data);
    }

    public function datatable()
    {
        $validated = request()->validate([
            'user->name' => 'nullable|array',
            'action' => 'nullable|array',
            'level' => 'nullable|array',
        ]);

        $query = AuditTrail::with('user:id,name')
            ->when($validated != [], function ($q) use ($validated) {
                filterData($q, $validated);
            })
            ->orderBy('created_at', 'desc');

        return datatables()->of($query)
            ->addColumn('button', function ($obj) {
                $button = "";
                if ($obj->level == 3 && $obj->old_data || $obj->new_data)
                    $button = '<a class="px-1 text-success btn-ajax-open-model-xl" data-title="Detail Audit Trail" href="' . route($this->route . 'show', $obj->id) . '" data-bs-toggle="tooltip" data-placement="top" data-bs-title="See Detail"><i class="far fa-eye"></i></a>';

                return $button;
            })
            ->editColumn('created_at', function ($obj) {
                return $obj->created_at->format('d-m-Y H:i:s');
            })
            ->rawColumns(['button'])
            ->make(true);
    }

    public function show($id)
    {
        $data['obj'] = AuditTrail::find($id);
        return $this->view('show', $data);
    }
}
