<?php

use Illuminate\Support\Facades\Route;
use Modules\AuditTrail\Http\Controllers\AuditTrailController;

Route::middleware(['auth', 'permission:view audit_trail'])->prefix('admin/audit-trail')->name('audit-trail.')->group(function () {
    Route::get('/', [AuditTrailController::class, 'index'])->name('index');
    Route::get('datatable', [AuditTrailController::class, 'datatable'])->name('datatable');
    Route::get('show/{id}', [AuditTrailController::class, 'show'])->name('show');
});
