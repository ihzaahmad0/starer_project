    <div class="row">
        <div class="col-md-12 table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Kolom</th>
                        <th>Old Value</th>
                        <th>New Value</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $longest_obj = count($data['obj']->old_data ?? []) > count($data['obj']->new_data ?? []) ? 'old_data' : 'new_data';
                    @endphp
                    @foreach ($data['obj']->$longest_obj as $key => $value)
                        <tr>
                            <td>{{ $key }}</td>
                            <td>
                                {{ $data['obj']->old_data[$key] ?? '' }}
                            </td>
                            <td>
                                {{ $data['obj']->new_data[$key] ?? '' }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
