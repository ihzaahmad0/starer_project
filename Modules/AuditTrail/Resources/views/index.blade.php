@extends('layouts.master')

@section('page_title', 'Audit Trail')


@section('breadcrumb')
    @php
        $breadcrumbs = ['Audit Trail'];
    @endphp
    @include('layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex">
                    <div class="head-label text-center">
                        <h5 class="card-title mb-0">Audit Trail</h5>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="main-table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User</th>
                                <th>Activity</th>
                                <th>IP Address</th>
                                <th>Level</th>
                                <th>Created At</th>
                                <th style="width: 7%">Action</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th>
                                    @include('layouts.datatables.filter.input', [
                                        'name' => 'user->name[il]',
                                        'placeholder' => 'name...',
                                    ])
                                </th>
                                <th>
                                    @include('layouts.datatables.filter.input', [
                                        'name' => 'action[il]',
                                        'placeholder' => 'activity...',
                                    ])
                                </th>
                                <th></th>
                                <th>
                                    @include('layouts.datatables.filter.select', [
                                        'name' => 'level[e]',
                                        'options' => $data['level'],
                                    ])
                                </th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('layouts.data_tables.basic_data_tables')

@include('layouts.modals.modal-xl')

@push('scripts')
    <script src="{{ asset('assets/js/datatables/init_datatable.js') }}"></script>
    <script src="{{ asset('assets/js/fetch_api/fetchPost.js') }}"></script>
    <script src="{{ asset('assets/js/utils/btn-delete-item-datatable.js') }}"></script>

    <script>
        $main_datatable = init_serverside_datatable(
            '#main-table',
            "{{ route($route . 'datatable') }}",
            [{
                    data: 'user.name',
                },
                {
                    data: 'action',
                    className: 'text-read-more'
                },
                {
                    data: 'ip',
                },
                {
                    data: 'level',
                },
                {
                    data: 'created_at',
                },
                {
                    data: 'button',
                    sortable: false,
                    className: "text-center"
                }
            ], {
                order: [3, 'desc'],
            }
        );
    </script>
@endpush
