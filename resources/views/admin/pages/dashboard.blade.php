@extends('layouts.master')

@section('page_title', 'Dashboard')


@section('breadcrumb')
    @php
    $breadcrumbs = ['Home', ['Dashboard', route('admin.dashboard.index')]];
    @endphp
    @include('layouts.parts.breadcrumb',['breadcrumbs'=>$breadcrumbs])
@endsection

@push('styles')

@endpush

@section('content')

@endsection

@push('scripts')

@endpush
