@push('modals')
    @php
        $modal_id = isset($modal_id) ? $modal_id : 'modal' . rand();
    @endphp
    <!-- Modal -->
    <div class="modal fade" id="{{ $modal_id }}" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
        aria-hidden="true">
        <div class="modal-dialog modal-{{ $size ?? 'md' }}" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        @php
            $trigger_el = isset($trigger_el) ? $trigger_el : '.btn-ajax-open-modal';
        @endphp
        <script>
            $(document).on('click', "{{ $trigger_el }}", async function() {
                event.preventDefault();
                let modal_id = "{{ $modal_id }}";
                loader.show();
                fetch($(this).attr('href'))
                    .then(resp => resp.text())
                    .then(text => {
                        $(`#${modal_id} .modal-title`).html($(this).data('title') ?? $(this).data('bs-title'));
                        $(`#${modal_id} .modal-body`).html(text);
                        $(`#${modal_id}`).modal('show');
                    })
                    .catch(err => {
                        Toast.error(err);
                    })
                    .finally(() => loader.hide())
            });
        </script>
    @endpush
@endpush
