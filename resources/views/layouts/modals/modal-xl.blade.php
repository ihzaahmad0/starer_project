@push('modals')
    <!-- Modal -->
    <div class="modal fade" id="{{ isset($id) ? $id : 'template-modal-xl' }}" tabindex="-1" role="dialog"
        aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            $(document).on('click', '.btn-ajax-open-model-xl', async function() {
                event.preventDefault();
                let modal_id = "{{ isset($id) ? $id : 'template-modal-xl' }}";
                loader.show();
                fetch($(this).attr('href'))
                    .then(resp => resp.text())
                    .then(text => {
                        $(`#${modal_id} .modal-title`).html($(this).data('title') ?? $(this).data('bs-title'));
                        $(`#${modal_id} .modal-body`).html(text);
                        $(`#${modal_id}`).modal('show');
                    })
                    .catch(err => {
                        Toast.error(err);
                    })
                    .finally(() => loader.hide())
            });
        </script>
    @endpush
@endpush
