<div class="input-group">
    <input name="{{ $name }}" class="form-control datatable-filter-date-picker" data-target="datatable-filter"
        placeholder="{{ isset($placeholder) ? $placeholder : 'cari...' }}" autocomplete="off"
        data-date-format="{{ isset($date_format) ? $date_format : 'dd-mm-yyyy' }}">
</div>
@once

@endonce
@once
    @include('layouts.datepicker.init')
    @push('scripts')
        <script src="{{ asset('assets/js/libs/luxon.min.js') }}"></script>
        <script>
            $(document).ready(() => {
                @include('layouts.datatables.filter.js_datepicker')

                $('.datatable-filter-date-picker').datepicker({
                    autoclose: true,
                    todayHighlight: true
                });
            })
        </script>
    @endpush
@endonce
