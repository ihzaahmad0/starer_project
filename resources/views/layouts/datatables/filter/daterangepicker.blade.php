<div class="input-daterange input-group">
    <input type="text" class="input-sm form-control datatable-filter-date-picker" name="{{ $name }}[gte]"
        data-date-format="{{ isset($date_format) ? $date_format : 'dd-mm-yyyy' }}" data-target="datatable-filter" />
    <div class="input-group-prepend">
        <span class="input-group-text">to</span>
    </div>
    <input type="text" class="input-sm form-control datatable-filter datatable-filter-date-picker"
        name="{{ $name }}[lte]" data-date-format="{{ isset($date_format) ? $date_format : 'dd-mm-yyyy' }}"
        data-target="datatable-filter" />
</div>
@once
    @include('layouts.datepicker.init')
    @push('scripts')
        <script src="{{ asset('assets/js/libs/luxon.min.js') }}"></script>
        <script>
            $(document).ready(() => {
                @include('layouts.datatables.filter.js_datepicker')
                $('.input-daterange').datepicker({
                    autoclose: true
                });
            })
        </script>
    @endpush
@endonce
