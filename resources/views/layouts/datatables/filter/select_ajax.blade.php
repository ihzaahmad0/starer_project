@php
    $id = 'datatable-filter-select-ajax-' . rand() + rand();
@endphp
<select class="datatable-filter-select-ajax form-control" id="{{ $id }}" name="{{ $name }}"
    data-target="datatable-filter" style="width: 100%">
</select>
@once
    @include('layouts.select2.init')
@endonce
@push('scripts')
    <script>
        $(document).ready(() => {
            $("#{{ $id }}").select2({
                theme: "bootstrap-5",
                placeholder: 'Cari',
                closeOnSelect: true,
                allowClear: true,
                ajax: {
                    url: "{{ $url }}",
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    data: function(term) {
                        return {
                            keyword: term.term,
                            page: term.page || 1,
                        };
                    },
                    processResults: function(data) {
                        return {
                            pagination: {
                                more: data.next_page_url != null,
                            },
                            results: data.data,
                        };
                    },
                    delay: 250
                }
            });
            $("#{{ $id }}").on('change', function() {
                let tableId = "#{{ isset($table_id) ? $table_id : '' }}";
                if (tableId == '#') {
                    tableId = "#main-table"
                }
                $(tableId).DataTable().draw();
            });
        })
    </script>
@endpush
