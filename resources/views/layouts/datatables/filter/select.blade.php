<select class="datatable-filter-select form-control" name="{{ $name }}" data-target="datatable-filter"
    style="width: 100%">
    <option value="" selected>Semua</option>
    @foreach ($options as $k => $v)
        <option value="{{ $k }}">{{ $v }}
        </option>
    @endforeach
</select>

@once
    @push('scripts')
        <script>
            $(document).ready(() => {
                $('.datatable-filter-select').select2({
                    theme: "bootstrap-5",
                });
                $('.datatable-filter-select').on('change', function() {
                    let tableId = "#{{ isset($table_id) ? $table_id : '' }}";
                    if (tableId == '#') {
                        tableId = "#main-table"
                    }
                    $(tableId).DataTable().draw();
                });
            })
        </script>
    @endpush
@endonce
