@prepend('styles')
    <link href="{{ asset('assets/css/libs/select2.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('assets/css/libs/select2-bootstrap-5-theme.min.css') }}" />
@endprepend
@prepend('scripts')
    <script src="{{ asset('assets/js/libs/select2.min.js') }}"></script>
@endprepend
