@prepend('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/libs/dataTables.bootstrap5.min.css') }}" />
@endprepend
@prepend('scripts')
    <script src="{{ asset('assets/js/libs/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/libs/dataTables.bootstrap5.min.js') }}"></script>
@endprepend
