@prepend('scripts')
    <link rel="stylesheet" href="{{ asset('assets/css/libs/bootstrap-datepicker.min.css') }}" />
    <script src="{{ asset('assets/js/libs/bootstrap-datepicker.min.js') }}"></script>
@endprepend
