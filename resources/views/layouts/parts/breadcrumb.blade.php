    <ol class="breadcrumb breadcrumb-style1 ml-auto">
        @foreach (isset($breadcrumbs) ? $breadcrumbs : [] as $breadcrumb)
            @if (gettype($breadcrumb) != 'array')
                <li class="breadcrumb-item @if ($loop->last) active @endif">
                    {{ $breadcrumb }}
                </li>
            @else
                <li class="breadcrumb-item @if ($loop->last) active @endif">
                    <a href="{{ $breadcrumb[1] }}">{{ $breadcrumb[0] }}</a>
                </li>
            @endif
        @endforeach
    </ol>
