<li class="menu-item {{ strpos(Route::current()->getName(), $item->data('prefix_route_name')) !== false ? 'active' : '' }}">
    <a class="menu-link"
        href="{{ $item->url() }}">
        @if ($item->data('icon'))
            <i class="menu-icon tf-icons {{ $item->data('icon') }}"></i>
        @endif
        <div>{{ $item->title }}</div>
    </a>
</li>
