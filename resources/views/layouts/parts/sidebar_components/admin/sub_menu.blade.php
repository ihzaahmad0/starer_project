<li
    class="menu-item {{ strpos(Route::current()->getName(), $item->data('prefix_route_name')) !== false ? 'active open' : '' }}">
    <a class="menu-link menu-toggle" data-toggle="submenu" aria-haspopup="true" aria-expanded="false"
        href="javascript:void(0);">
        <i class="menu-icon tf-icons {{ $item->data('icon') }}"></i>
        <div>{{ $item->title }}</div>
    </a>
    <ul class="menu-sub">
        @foreach ($item->children() as $children)
            @if ($children->hasChildren())
                @include('layouts.parts.sidebar_components.admin.sub_menu', [
                    'item' => $children,
                ])
            @else
                @include('layouts.parts.sidebar_components.admin.nav-item', ['item' => $children])
            @endif
        @endforeach
    </ul>
</li>
