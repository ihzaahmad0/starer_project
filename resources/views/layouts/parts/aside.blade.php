<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo">
        <a href="" class="app-brand-link w-100">
            <span class="app-brand-logo w-100">
                <img src="{{ asset('assets/img/elements/navbar-logo-danareksa.png') }}" class="w-100" alt="">
            </span>
            {{-- <span class="app-brand-text demo menu-text fw-bolder ms-2">{{ config('app.name') }}</span> --}}
        </a>

        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
            <i class="bx bx-chevron-left bx-sm align-middle"></i>
        </a>
    </div>

    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py-1">
        @include('layouts.parts.sidebar_components.admin.index')
    </ul>
</aside>
