<script>
    const loader = {
        show() {
            $('#page-loader').css('display', 'block');
        },
        hide() {
            $('#page-loader').css('display', 'none')
        }
    };
</script>
