@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/libs/lightbox.min.css') }}" />
@endpush

@push('scripts')
    <script src="{{ asset('assets/js/libs/lightbox.min.js') }}"></script>
@endpush
