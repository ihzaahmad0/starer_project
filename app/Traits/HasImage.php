<?php

namespace App\Traits;

use App\Models\User;
use App\Utils\Constants;
use App\Utils\ImageUploadHelper;
use Illuminate\Support\Facades\Storage;
use Image;

trait HasImage
{
    private static $imageAttribute = 'image';

    public function hasImage($attribute = null)
    {
        $fieldName = is_null($attribute) ? $this->imageAttribute : $attribute;
        return !!($this->$fieldName);
    }

    public function image($attribute = null, $relative_path = false)
    {
        $fieldName = is_null($attribute) ? $this->imageAttribute : $attribute;
        if (!$this->$fieldName) {
            if ($relative_path) {
                return 'assets/img/avatars/default.png';
            }
            return asset('assets/img/avatars/default.png');
        }
        if ($relative_path) {
            return Storage::url($this->$fieldName);
        }
        return asset(Storage::url($this->$fieldName));
    }

    public function imageHtml($attribute = null, $asThumbnail = true)
    {
        if (!$this->hasImage($attribute)) {
            return '---';
        }

        if ($asThumbnail) {
            return '<a target="_blank" href="' . $this->image($attribute) . '">
                <img alt="" src="' . $this->thumbnail($attribute) . '" class="img-responsive" />
            </a>';
        }

        return '<img alt="" src="' . $this->image($attribute) . '" class="img-responsive" />';
    }

    public function imageBase64($attribute = null)
    {
        $img = Image::make($this->image($attribute));
        return $img->encode('data-url');
    }

    public function thumbnail($attribute = null, $relative_path = false)
    {
        $fieldName = is_null($attribute) ? $this->imageAttribute : $attribute;
        if ($this->$fieldName) {
            $path_parts = pathinfo($this->$fieldName);
            if (Storage::disk('public')->exists($path_parts['dirname'] . '/' . $path_parts['filename'] . '.thumb.' . $path_parts['extension'])) {
                if ($relative_path) {
                    return Storage::url($path_parts['dirname'] . '/' . $path_parts['filename'] . '.thumb.' . $path_parts['extension']);
                }
                return asset(Storage::url($path_parts['dirname'] . '/' . $path_parts['filename'] . '.thumb.' . $path_parts['extension']));
            } elseif (Storage::disk('public')->exists($this->$fieldName)) {
                if ($relative_path) {
                    return Storage::url($this->$fieldName);
                }
                return asset(Storage::url($this->$fieldName));
            }
        }
        if ($relative_path) {
            return 'assets/img/avatars/default.png';
        }
        return asset('assets/img/avatars/default.png');
    }

    public function upload_image($file, $path, $quality)
    {
        return ImageUploadHelper::upload($file, $path, $quality);
    }
}
