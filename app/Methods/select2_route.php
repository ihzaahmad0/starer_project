<?php

// Do exactly like this file to create global method
// Just change the method name
// The best practice is using underscore (_) as prefix the method name
// Best Practice Class Name Example: _sendEmail, _deleteData, ect.

use App\Models\BaseModel;
use Illuminate\Support\Facades\DB;

if (!function_exists('select2_route')) {
    function select2_route($model, $id = 'id', $text = "name")
    {
        $select = [
            DB::raw($id . ' as id'),
        ];
        // Check if $text is an array
        if (is_array($text)) {
            // Construct the CONCAT_WS expression to concatenate columns with ' - ' delimiter
            $concatenatedText = 'CONCAT_WS(\' - \', ' . implode(', ', $text) . ') as text';
            $select[] = DB::raw($concatenatedText);
        } elseif (is_string($text)) {
            // If $text is a string, simply add it as a raw expression
            $select[] = DB::raw($text . ' as text');
        }
        $data = $model::when(gettype($text) == "string", function ($q) use ($text) {
            return $q->where($text, 'ILIKE', '%' . request()->keyword . '%');
        })
            ->when(gettype($text) == "array", function ($q) use ($text) {
                return $q->where(function ($query) use ($text) {
                    foreach ($text as $key => $value) {
                        $query->orWhere($value, 'ILIKE', '%' . request()->keyword . '%');
                    }
                });
            })
            ->select($select);

        foreach (request()->where ?? [] as $column => $value) {
            $search = explode(':', $value);
            if (isset(BaseModel::OPERATORS[$search[0]]))
                $data = $data->where($column, BaseModel::OPERATORS[$search[0]], $search[1]);
            elseif (isset(BaseModel::OPERATORS_IN[$search[0]]))
                $data = $data->whereIn($column, BaseModel::OPERATORS_IN[$search[0]], explode(',', $search[1]));
            elseif (isset(BaseModel::OPERATORS_NOT_IN[$search[0]]))
                $data = $data->whereNotIn($column, explode(',', $search[1]));
        }
        $data = $data->paginate()->withQueryString();
        return $data;
    }
}
