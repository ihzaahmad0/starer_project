<?php

namespace App\Utils;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Image;

/**
 * Upload Image and Resize (if needed)
 *
 * This class can used for uploading image dan resize the size (optional) with keep the aspec ratio.
 *
 * @copyright  2021 Ihza
 * @license
 * @version     1.0
 * @link
 * @since      Class available since Release 1.0
 */
class ImageUploadHelper
{
    /**
     * Upload the image
     *
     * @param Illuminate\Http\Request   $imageRequestObject  the image object from request object
     * @param integer $requiredSize the max size for uploaded image. if don't want to resize the image, just give this false
     * @param string $path where the image will be placed
     * @param string $name the name of uploaded image
     * @param boolean $generateThumbnail condition to generate the thumbnail or not
     *
     * @throws None
     * @author Ihza
     * @return array contain full path the uploaded image
     */
    public static function upload($imageRequestObject, $path, $quality, $name = null, $generateThumbnail = true, $requiredSize = false)
    {
        $image = Image::make($imageRequestObject)->stream("webp", $quality);
        $original_image = Image::make($imageRequestObject);
        $ext = '.webp';
        $width = $original_image->width();
        $height = $original_image->height();
        if (!$name) {
            $str = rand() - rand();
            $result = md5($str);
            $name = $result . Carbon::now()->format('Ymdhis');
        }
        $filename = $path . '/' . $name . $ext;
        // Check if image resize is required or not
        if ($requiredSize >= $width && $requiredSize >= $height || $requiredSize == false) {
            Storage::disk('public')->put($filename, $image);
            $data['full_path'] = $filename;
            $data['resize'] = false;
        } else {

            $newWidth = 0;
            $newHeight = 0;

            $aspectRatio = $width / $height;
            if ($aspectRatio >= 1.0) {
                $newWidth = $requiredSize;
                $newHeight = $requiredSize / $aspectRatio;
            } else {
                $newWidth = $requiredSize * $aspectRatio;
                $newHeight = $requiredSize;
            }

            $resize_image = $original_image;
            $resize_image->resize($newWidth, $newHeight)->stream("webp", $quality);
            Storage::disk('public')->put($filename, $resize_image);
            $data['resize'] = true;
        }
        if ($generateThumbnail) {
            $filename_thumb = $name . '.thumb' . $ext;
            $relativePath_thumb = $path . '/' . $filename_thumb;

            $thumbnail_image = $original_image;
            $thumbnail_image->fit(150, 150)->stream("webp", 75);
            $thumbnail = Storage::disk('public')->put($relativePath_thumb, $thumbnail_image);
            $data['thumbnail'] = $thumbnail;
        }
        $data['status'] =  true;

        return $data;
    }
}
