<?php

namespace App\Utils;

use Lavary\Menu\Facade as Menu;

class MenuGenerator
{
    public static function generateMenu()
    {
        // documentation https://github.com/lavary/laravel-menu
        $menu = Menu::make('side_menu', function ($menu) {

            // DASHBOARD
            $menu->add('Dashboard', route('admin.dashboard.index'))
                ->data('prefix_route_name', 'admin.dashboard.')
                ->data('icon', 'bx bxs-dashboard');

            // Pengaturan User Start <<<<<
            // add parent menu
            $menu
                ->add('User Config')
                ->nickname('pengaturan_user')
                ->data('prefix_route_name', 'admin.user_config.')
                ->data('icon', 'fas fa-circle-user');
            // add sub menu
            $menu
                ->pengaturan_user
                ->add('Role', route('admin.user_config.role.index'))
                ->data('prefix_route_name', 'admin.user_config.role.')
                ->data('permission', 'view user_config.role');
            // add sub menu
            $menu
                ->pengaturan_user
                ->add('User', route('admin.user_config.user.index'))
                ->data('prefix_route_name', 'admin.user_config.user.')
                ->data('permission', 'view user_config.user');
            // Pengaturan User End <<<<<

            // audit trail
            $menu->add('Audit Trail', route('audit-trail.index'))
                ->data('prefix_route_name', 'audit-trail.')
                ->data('permission', 'view audit_trail')
                ->data('icon', 'bx bx-check-shield');
            // audit trail

            // master
            $menu
                ->add('Master')
                ->nickname('master')
                ->data('prefix_route_name', 'master.')
                ->data('icon', 'fa fa-database');
            // add sub menu
            $menu
                ->master
                ->add('Utilitas', route('master.utilitas.index'))
                ->data('prefix_route_name', 'master.utilitas.')
                ->data('permission', 'view master.utilitas');
            $menu
                ->master
                ->add('Jenis Usaha', route('master.jenis_usaha.index'))
                ->data('prefix_route_name', 'master.jenis_usaha.')
                ->data('permission', 'view master.jenis_usaha');
            $menu
                ->master
                ->add('Status Karyawan', route('master.status_karyawan.index'))
                ->data('prefix_route_name', 'master.status_karyawan.')
                ->data('permission', 'view master.status_karyawan');
            $menu
                ->master
                ->add('Satuan', route('master.satuan.index'))
                ->data('prefix_route_name', 'master.satuan.')
                ->data('permission', 'view master.satuan');
            $menu
                ->master
                ->add('Jabatan', route('master.jabatan.index'))
                ->data('prefix_route_name', 'master.jabatan.')
                ->data('permission', 'view master.jabatan');
            $menu
                ->master
                ->add('Identitas', route('master.identitas.index'))
                ->data('prefix_route_name', 'master.identitas.')
                ->data('permission', 'view master.identitas');
            $menu
                ->master
                ->add('Supplier', route('master.supplier.index'))
                ->data('prefix_route_name', 'master.supplier.')
                ->data('permission', 'view master.supplier');
            $menu
                ->master
                ->add('Layanan', route('master.layanan.index'))
                ->data('prefix_route_name', 'master.layanan.')
                ->data('permission', 'view master.layanan');
            $menu
                ->master
                ->add('Entitas', route('master.entitas.index'))
                ->data('prefix_route_name', 'master.entitas.')
                ->data('permission', 'view master.entitas');
            // add sub menu



        })->filter(function ($item) {
            if ($item->hasParent()) {
                $delete_if_all_false = $item->parent()->data('delete_if_all_false') ?? [];
            }
            if ($item->data('permission') == null || auth()->user()->can($item->data('permission'))) {
                if ($item->hasParent()) {
                    array_push($delete_if_all_false, true);
                    $item->parent()->data('delete_if_all_false', $delete_if_all_false);
                }
                return true;
            }
            if ($item->hasParent()) {
                array_push($delete_if_all_false, false);
                $item->parent()->data('delete_if_all_false', $delete_if_all_false);
            }
            return false;
        })->filter(function ($item) {
            $delete_if_all_false = $item->data('delete_if_all_false') ?? [true];
            if (in_array(true, $delete_if_all_false)) {
                return true;
            }
            return false;
        });

        return $menu->roots();
    }
}
