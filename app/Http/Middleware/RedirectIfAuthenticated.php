<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use App\Utils\FlashMessageHelper;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $user = auth()->user();

        if ($user) {
            FlashMessageHelper::alert(['icon' => 'info', 'title' => 'Anda telah login!']);
            return redirect(route('admin.dashboard.index'));
        }

        return $next($request);
    }
}
