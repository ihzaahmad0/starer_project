<?php

namespace App\Observers;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use ReflectionClass;
use ReflectionMethod;

class UserStampObserver
{
    protected $userId = 0;

    public function __construct()
    {
        $user = auth()->user();
        if ($user) {
            $this->userId = $user->id;
        }
    }
    /**
     * Handle the User "created" event.
     *
     * @return void
     */
    public function creating($model)
    {
        $model->created_by = $this->userId;
        $model->updated_by = $this->userId;
        // if user login
        if (auth()->check()) {
            auth()->user()->storeLog(3, 'Create Data ' . get_class($model), $model->getAttributes());
        }
    }

    /**
     * Handle the User "updated" event.
     *
     * @return void
     */
    public function updating($model)
    {
        // $model->updated_at = Carbon::now();

        // compare current data and old data to get the difference
        $old = $model->getOriginal();
        $difference = array_diff_assoc($model->getAttributes(), $old);
        // get only $key from $old
        $old = array_intersect_key($old, $difference);
        if (auth()->check()) {
            auth()->user()->storeLog(3, 'Update Data ' . get_class($model) . ' with id:' . $model->id, $difference, $old);
        }

        $model->updated_by = $this->userId;
    }

    // public function updated($model)
    // {
    //     $model->updated_by = $this->userId;
    // }

    /**
     * Handle the User "deleted" event.
     *
     * @return void
     */
    public function deleting(Model $model)
    {
        $relations = $model->check_on_delete_relations ?? [];

        foreach ($relations as $relationName) {
            if ($model->$relationName()->exists()) {
                $relationName = ucfirst(str_replace('_', ' ', $relationName));
                throw new Exception("Tidak bisa menghapus data ini. Data ini digunakan oleh " . $relationName);
            }
        }
    }


    public function deleted($model)
    {
        if (auth()->check()) {
            auth()->user()->storeLog(3, 'Delete Data ' . get_class($model) . ' with id: ' . $model->id);
        }
        if ($model->isForceDeleting()) {
            $model->forceDelete();
        } else {
            $model->deleted_by = $this->userId;
            $model->save();
        }
    }

    public function restoring($model)
    {
        $model->restored_by = $this->userId;
        $model->restored_at = Carbon::now();
        $model->deleted_by = "";
        $model->deleted_at = "";
        return true;
    }
}
