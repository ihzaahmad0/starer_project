<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Database\Eloquent\Model;

class UniqueSoftDelete implements ValidationRule
{
    /**
     * The model to check for uniqueness.
     *
     * @var Model
     */
    private $model;

    /**
     * The ID to exclude from the uniqueness check.
     *
     * @var mixed|null
     */
    private $excludeId;

    /**
     * Create a new rule instance.
     *
     * @param string $table
     * @param mixed|null $excludeId
     */
    public function __construct(string $modelClass, $excludeId = null)
    {
        $this->model = app($modelClass);
        $this->excludeId = $excludeId;
    }

    /**
     * Run the validation rule.
     *
     * @param string $attribute
     * @param mixed $value
     * @param Closure(string): \Illuminate\Translation\PotentiallyTranslatedString $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $query = $this->model->where($attribute, $value)->whereNull('deleted_at');

        // If the value being validated has not changed, consider it valid.
        if ($this->model->$attribute == $value) {
            return;
        }

        if ($this->model->exists) {
            $query->where('id', '!=', $this->model->getKey());
        }

        // If there's an ID to exclude, exclude it from the uniqueness check.
        if ($this->excludeId) {
            $query->where('id', '!=', $this->excludeId);
        }

        if ($query->exists()) {
            $fail("The $attribute has already been taken.");
        }
    }
}
