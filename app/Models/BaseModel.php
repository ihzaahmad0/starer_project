<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Observers\UserStampObserver;
use App\Traits\CanGetTableNameStatically;
use App\Traits\UserStamp;
use App\Utils\Date\LocalCarbon;
use Carbon\Carbon;
use Exception;

class BaseModel extends Model
{
    use HasFactory,  CanGetTableNameStatically, UserStamp, SoftDeletes;
    const OPERATORS = [
        'e' => '=',
        'ne' => '!=',
        'gt' => '>',
        'gte' => '>=',
        'lt' => '<',
        'lte' => '<=',
        'l' => 'LIKE',
        'il' => 'ILIKE',
        'is' => 'IS',
    ];

    const OPERATORS_IN = [
        'nei' => '='
    ];

    const OPERATORS_NOT_IN = [
        'neni' => '!=', //not equal not in
    ];

    const RELATION_OPERATORS = [
        're' => '=',
        'rne' => '!=',
        'rgt' => '>',
        'rgte' => '>=',
        'rlt' => '<',
        'rlte' => '<=',
        'rl' => 'LIKE',
        'ril' => 'ILIKE',
        'ris' => 'IS',
    ];

    // this can make vulnerability
    // protected $guarded = [];
    public function createdByUser()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function creator_name()
    {
        return optional($this->createdByUser()->first())->name ?? '-';
    }
    public function updatedByUser()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function updater_name()
    {
        return optional($this->updatedByUser()->first())->name ?? '-';
    }

    public function deletedByUser()
    {
        return $this->belongsTo(User::class, 'deleted_by');
    }
    public function restoredByUser()
    {
        return $this->belongsTo(User::class, 'restored_by');
    }

    public function getFormattedCreatedAtAttribute()
    {
        return Carbon::parse($this->created_at)->format('h:i / d-m-Y ');
    }

    public function getFormattedDate($column, $format = 'd/m/Y H:i:s')
    {
        try {
            return Carbon::parse($this->$column, 'Asia/Jakarta')->format($format);
        } catch (Exception $e) {
            return Carbon::parse($this->getAttributes()[$column], 'Asia/Jakarta')->format($format);
        }
    }
}
