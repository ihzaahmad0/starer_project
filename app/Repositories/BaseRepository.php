<?php

namespace App\Repositories;

class BaseRepository
{
    protected $obj, $route, $view, $permission;

    public function setProperty($prop)
    {
        $this->route = optional($prop)['route'];
        $this->view = optional($prop)['view'];
        $this->permission = optional($prop)['permission'];
    }

    public function getById($id)
    {
        return $this->obj->findOrFail($id);
    }

    public function storeFromRequest($data)
    {
        return $this->obj->create($data);
    }

    public function updateFromRequest($id, $data)
    {
        return $this->getById($id)->update($data);
    }

    public function destroy($id)
    {
        return $this->getById($id)->delete();
    }

    public function restore($id)
    {
        return $this->getById($id)->restore();
    }
}
