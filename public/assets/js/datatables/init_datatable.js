const datatable_ajax_data = (d) => {
    $(`[data-target*='datatable-filter']`).each((i, element) => {
        d[$(element).attr("name")] = $(element).val();
    });
};
const shortless_drawcallback = (table) => {
    $(table).find('tbody').find('.text-read-more').map(function () {
        str = $(this).html();
        if (str.length > 50) {
            str.substr(0, 49) +
                '...<a href="#" data-str="" class="moreless">Selanjutnya</a>'
            var shh = `
                    <span>` + str.substr(0, 49) + `</span><span class="dots">...</span><span class="nextless" style="display:none;">` + str.substr(49, str.length) + `</span>
                    <span class="btn-toggle-readmore cursor-pointer" style="color:blue;">More</span>
                `;
            $(this).html(shh);
        }
    }).get();

    $('.btn-toggle-readmore').on('click', function () {
        if ($(this).text() == 'More') {
            $(this).parent().find('.dots').hide();
            $(this).parent().find('.nextless').show();
            $(this).text('Less').css('color', 'red')
        } else {
            $(this).parent().find('.dots').show();
            $(this).parent().find('.nextless').hide();
            $(this).text('More').css('color', 'blue')
        }
    });
}

let $datatable = {
    reload() {
        this.obj.draw();
    },
};
const init_serverside_datatable = (el, url, columns, options = {}) => {
    $datatable.obj = $(el).DataTable({
        bSortCellsTop: true,
        processing: true,
        serverSide: true,
        searching: false,
        ajax: {
            url: url,
            data: datatable_ajax_data,
        },
        columns: [
            {
                data: null,
                sortable: false,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
            },
            ...columns,
        ],
        drawCallback: function(settings) {
            shortless_drawcallback($(el));
        },
        lengthChange: false,
        ...options,
    });

    return $datatable.obj;
};

const init_datatable = (el, url, columns, options = {}) => {
    $(el).DataTable({
        bSortCellsTop: true,
        searching: true,
        ...options,
    });
};
